﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


#if UNITY_EDITOR
using UnityEditor;
#endif  
public class BasicCoinTracker : MonoBehaviour
{
    public static CurrencyManager<CurrencyType> _coinMan;
    public static CurrencyManager<CurrencyType> coinMan
    {
        get
        {
            if (_coinMan == null)
            {
                _coinMan = new CurrencyManager<CurrencyType>();
                _coinMan.EnsureInit(CurrencyType.COIN, SpecialBuildManager.initialCoinBonus);
            }
            return _coinMan;
        }
    }


    public Text coinDisplayText;
    public CurrencyType currencyType = CurrencyType.COIN;
    public float timeToAnimate = 1;
    public float stepTime = 1/20.0f;

    int savedValue;
    int displayValue;
    // Start is called before the first frame update

    public static int CoinBalance
    {
        get
        {
            return coinMan.GetBalance(CurrencyType.COIN);
        }
    }
    public static void ChangeCoin(int value)
    {
        if (SpecialBuildManager.disableCoinReduction && value < 0) return; 
        coinMan.ChangeBy(CurrencyType.COIN, value);
    }

    void OnEnable()
    {
        savedValue = coinMan.GetBalance(currencyType);
        coinDisplayText.text = savedValue.ToString();
        displayValue = savedValue;
        if (runningTextRoutine != null)
        {
            StopCoroutine(runningTextRoutine);
            runningTextRoutine = null;
        }
        coinMan.AddListner_BalanceChanged(currencyType, OnCoinChange);
    }
    void OnCoinChange()
    {
        savedValue = coinMan.GetBalance(currencyType);
        if (runningTextRoutine != null) StopCoroutine(runningTextRoutine);
        runningTextRoutine = StartCoroutine(TextRoutine());
    }

    private void OnDisable()
    {
        coinMan.RemoveListner_BalanceChanged(currencyType, OnCoinChange);
    }

    Coroutine runningTextRoutine;
    IEnumerator TextRoutine()
    {
        float startTime = Time.realtimeSinceStartup;
        int stepCount = Mathf.RoundToInt( timeToAnimate / this.stepTime);

        float stepTime = timeToAnimate / stepCount;
        int diff = savedValue - displayValue;
        int initialValue = displayValue;
        for (int i = 0; i < stepCount; i++)
        {
            yield return new WaitForSeconds(stepTime);

            float p = Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / timeToAnimate);
            displayValue = initialValue +  Mathf.RoundToInt(diff * p);
            coinDisplayText.text = displayValue.ToString();
        }
        displayValue = savedValue;
        coinDisplayText.text = displayValue.ToString();
    }
}
public enum CurrencyType
{
    COIN = 0,
}

#if UNITY_EDITOR
[CustomEditor(typeof(BasicCoinTracker))]
public class PrankCoinTrackerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        BasicCoinTracker pct = (BasicCoinTracker)target;
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("+20000"))
        {
            BasicCoinTracker.coinMan.ChangeBy(pct.currencyType, 20000);
        }
        if (GUILayout.Button("-10000"))
        {
            BasicCoinTracker.coinMan.ChangeBy(pct.currencyType, -100000);
        }

        EditorGUILayout.EndHorizontal();
    }
}
#endif
