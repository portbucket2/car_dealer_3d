﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaptureScreenShot : MonoBehaviour
{

    public KeyCode key = KeyCode.Insert;
    public string fileName;



    public int count;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(key))
        {
            ScreenCapture.CaptureScreenshot(fileName+count++ + ".png");
        }
    }
}
