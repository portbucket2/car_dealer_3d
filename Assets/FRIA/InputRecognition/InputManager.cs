﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    const float defWidth = 900;
    public float defPixCount = 15;
    public event System.Action<Dir> onSwipe;
    float confirmationPixCount;

    public static InputManager instance;
    Gesture currentGesture = null;
    private void Awake()
    {
        instance = this;
        confirmationPixCount =  (defPixCount/defWidth)* Screen.width;
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            currentGesture = new Gesture(new Vector2(Input.mousePosition.x, Input.mousePosition.y), confirmationPixCount);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            currentGesture = null;
        }

        if (currentGesture != null && currentGesture.type == GestureType.PROCESSING)
        {
            if (currentGesture.ProcessInputAndReturn_IsReady(new Vector2(Input.mousePosition.x, Input.mousePosition.y)))
            {
                switch (currentGesture.type)
                {
                    case GestureType.SWIPE_UP:
                        if (onSwipe != null) onSwipe(Dir.UP);
                        Debug.LogWarning("Up");
                        break;
                    case GestureType.SWIPE_DOWN:
                        if (onSwipe != null) onSwipe(Dir.DOWN);
                        Debug.LogWarning("Down");
                        break;
                    case GestureType.SWIPE_LEFT:
                        if (onSwipe != null) onSwipe(Dir.LEFT);                    
                        
                        Debug.LogWarning("Left");
                        break;
                    case GestureType.SWIPE_RIGHT:
                        if (onSwipe != null) onSwipe(Dir.RIGHT);
                        
                        Debug.LogWarning("Right");
                        break;
                    default:
                        Debug.LogWarning("UnrecognizedInput");
                        break;
                }
            }
        }
    }
}

public class Gesture
{
    const float maxTime = 0.4f;
    public GestureType type = GestureType.PROCESSING;
    Vector2 startPos;
    float startTime;
    float maxPixels = 100;
    public Gesture(Vector2 startPosition, float maxPixels)
    {
        this.maxPixels = maxPixels;
        startTime = Time.time;
        startPos = startPosition;
    }

    public bool ProcessInputAndReturn_IsReady(Vector2 currentPosition)
    {
        if (type!=GestureType.PROCESSING)
            return false;

        float dist = (currentPosition - startPos).magnitude;

        if (dist >= maxPixels)//'maxPixels' is the minimum amount of pixel, greater than which we will take swipe
        {
            float dX = currentPosition.x - startPos.x;
            float dY = currentPosition.y - startPos.y;
            if (Mathf.Abs(dX) > Mathf.Abs(dY))
            {
                if (dX > 0)
                    type = GestureType.SWIPE_RIGHT;
                else
                    type = GestureType.SWIPE_LEFT;
            }
            else
            {
                if (dY > 0)
                    type = GestureType.SWIPE_UP;
                else
                    type = GestureType.SWIPE_DOWN;
            }
            return true;
        }

        if (Time.time - startTime > maxTime)
        {
            type = GestureType.UNRECOGNIZED;
            return true;
        }
        else return false;
    }
}

public enum GestureType
{
    UNRECOGNIZED = -1,
    PROCESSING =0,
    SWIPE_UP,
    SWIPE_DOWN,
    SWIPE_LEFT,
    SWIPE_RIGHT,
}

public enum Dir
{
    UP,
    DOWN,
    LEFT,
    RIGHT,
}