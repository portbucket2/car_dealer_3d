﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMinigame
{
    void ActivateMinigame();
    void DeactivateMinigame();
    void Reset();
    event Action OnMinigameEnd;
}
