﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LionKitManager : MonoBehaviour
{
    public static LionKitManager Instance;

    private void Awake()
    {
        Singleton();
    }
    
    private void Singleton()
    {
        if (Instance != null)
        {
            Destroy(gameObject); return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public void LevelStarted(int t_LevelNumber)
    {
        Debug.LogWarning(t_LevelNumber + " BE4 started");
        LionStudios.Analytics.Events.LevelStarted(t_LevelNumber);
    }

    public void LevelCompleted(int t_LevelNumber)
    {
        Debug.LogWarning(t_LevelNumber + " Ending");
        LionStudios.Analytics.Events.LevelComplete(t_LevelNumber);
    }
}
