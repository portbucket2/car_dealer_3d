﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CharController : MonoBehaviour
{
    public static CharController instance;

    [SerializeField]
    private Ease linear;
    [SerializeField]
    private Ease inSine;

    private int CAR_ENTRY = Animator.StringToHash("entry");
    private int CAR_IDLE = Animator.StringToHash("idle");
    private int CAR_EXIT = Animator.StringToHash("exit");
    private int ACCEPT = Animator.StringToHash("accept");
    private int REJECT = Animator.StringToHash("reject");
    private int WALK = Animator.StringToHash("walk");
    private int WAIT = Animator.StringToHash("wait");
    private int KEY = Animator.StringToHash("key");
    private int THIEF = Animator.StringToHash("thief");
    private int ARREST = Animator.StringToHash("arrest");
    private int ARREST_WALK = Animator.StringToHash("arrest_walk");

    [SerializeField]
    private Vector3[] charPos;
    [SerializeField]
    private Vector3[] charRot;

    [SerializeField]
    private float moveDuration;
    [SerializeField]
    private float rotDuration;

    public bool charEntered;
    public bool charExited;
    public bool seqOneFinished;
    public bool seqTwoFinished;
    public bool charAccepted;
    public bool charRejected;



    [SerializeField]
    public Animator charAnim;

    public static CharController Instance
    {
        get { return instance; }
    }
    

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        /*else
        {
            Destroy(gameObject);
            return;
        }*/
    }
    void Start()
    {
        charAnim = GetComponent<Animator>();
        charEntered = false;
        seqOneFinished = false;
        seqTwoFinished = false;
        charExited = false;
        charAccepted = false;
        charRejected = false;
    }

    // Update is called once per frame
    public void TriggerAnimation(int trigger)
    {
        charAnim.SetTrigger(trigger);
        
            charAnim.speed = 2f;

    }

    public void EnteyCharacter()
    {
        StartCoroutine(EntrySequence());
    }

    public void ExitCharacter()
    {
        StartCoroutine(RejectSequence());
    }

    public void AcceptCharacter()
    {
        StartCoroutine(AcceptSequence());
    }

    public void BuyerEntry()
    {
        StartCoroutine(BuyerEntrySequence());
    }
    public void ThiefEntry()
    {
        StartCoroutine(ThiefSequence());
    }

    public void ThiefUpSwipeBehavior()
    {
        StartCoroutine(UpSwipeSequence());

    }

    IEnumerator UpSwipeSequence()
    {
        yield return new WaitForEndOfFrame();

        TriggerAnimation(REJECT);
        yield return new WaitForSeconds(1f);
        Sequence mySequence = DOTween.Sequence();
        mySequence.OnStart(()=> TriggerAnimation(ARREST))
                  .Append(transform.DORotate(charRot[3], .2f))
                  .Append(transform.DOMove(charPos[5], 1.5f)).OnComplete(()=> charAccepted = true);
        
        
    }

    

    IEnumerator ThiefSequence()
    {
        yield return new WaitForEndOfFrame();

        Sequence mySequence = DOTween.Sequence();
        TriggerAnimation(CAR_IDLE);
        mySequence.Append(transform.DOMove(charPos[0], moveDuration))
                  .Append(transform.DOMove(charPos[1], moveDuration))
                  .Join(transform.DORotate(charRot[0], rotDuration).SetEase(inSine))
                  .PrependInterval(1)
                  .Append(transform.DOMove(charPos[2], moveDuration).SetEase(inSine)).OnStepComplete(() => TriggerAnimation(CAR_EXIT))
                  .SetAutoKill(true).OnComplete(() => seqOneFinished = true);


        yield return new WaitUntil(() => seqOneFinished == true);
        seqOneFinished = false;
        yield return new WaitForSeconds(2.5f);
        TriggerAnimation(WALK);
        Sequence nextSequence = DOTween.Sequence();
        nextSequence.Append(transform.DORotate(charRot[1], .2f)).OnStepComplete(() => TriggerAnimation(THIEF))
                    .SetDelay(0)
                    .Append(transform.DOMove(charPos[3], 1f))
                    .Insert(1f, transform.DORotate(charRot[2], .2f))
                    .Append(transform.DOMove(charPos[4], 1f))
                    .Append(transform.DORotate(charRot[1], .2f))

                    .SetAutoKill(true).OnComplete(() => charExited = true);

    }

    IEnumerator BuyerEntrySequence()
    {
        yield return new WaitForEndOfFrame();
        Sequence sequence = DOTween.Sequence();
        TriggerAnimation(WALK);

        sequence.Append(transform.DOMove(charPos[0], moveDuration))
                .Append(transform.DOMove(charPos[1], moveDuration))
                .Join(transform.DORotate(charRot[0], rotDuration).SetEase(inSine))
                .PrependInterval(1)
                .Append(transform.DOMove(charPos[4], 2f).SetEase(Ease.Linear)).OnComplete(() => BuyerEnd());
        
    }
    void BuyerEnd()
    {
        TriggerAnimation(WAIT);
        charExited = true;
    }


    public IEnumerator EntrySequence()
    {
        Sequence mySequence = DOTween.Sequence();
        TriggerAnimation(CAR_IDLE);
        mySequence.Append(transform.DOMove(charPos[0], moveDuration))
                  .Append(transform.DOMove(charPos[1], moveDuration))
                  .Join(transform.DORotate(charRot[0], rotDuration).SetEase(inSine))
                  .PrependInterval(1)
                  .Append(transform.DOMove(charPos[2], moveDuration).SetEase(inSine)).OnStepComplete(() =>TriggerAnimation(CAR_EXIT))
                  .SetAutoKill(true).OnComplete(() => seqOneFinished = true);


        yield return new WaitUntil(() => seqOneFinished == true);
        seqOneFinished = false;
        yield return new WaitForSeconds(2.5f);
        TriggerAnimation(WALK);
        Sequence nextSequence = DOTween.Sequence();
        nextSequence.Append(transform.DORotate(charRot[1], .2f)).OnStepComplete(()=> TriggerAnimation(WAIT))
                    .SetDelay(0)
                    .Append(transform.DOMove(charPos[3], 1f))
                    .Insert(1f, transform.DORotate(charRot[2], .2f))
                    .Append(transform.DOMove(charPos[4], 1f))
                    .Append(transform.DORotate(charRot[1],.2f))
                    
                    .SetAutoKill(true).OnComplete(() => charExited = true);


    }

    IEnumerator AcceptSequence()
    {
        yield return new WaitForEndOfFrame();
        if (CDGameplay.Instance.isBuyer)
        {
            TriggerAnimation(REJECT);

        }
        else
        {
            
            TriggerAnimation(ACCEPT);
        }
       


        yield return new WaitForSeconds(1f);
        TriggerAnimation(WALK);

        Sequence mySequence = DOTween.Sequence();
        mySequence.Append(transform.DORotate(charRot[3], .2f))
                  .Append(transform.DOMove(charPos[5], 1.5f)).OnComplete(()=> charAccepted = true);
    }

    IEnumerator RejectSequence()
    {
        yield return new WaitForEndOfFrame();
        if(CDGameplay.Instance.isBuyer)
        {
            TriggerAnimation(ACCEPT);

        }
        else
        {
            TriggerAnimation(REJECT);
        }

        if (CDGameplay.Instance.isBuyer)
        {
            yield return new WaitForSeconds(2f);
        }
        else
        {
            yield return new WaitForSeconds(2f);
        }
        TriggerAnimation(WALK);
        Sequence mySequence = DOTween.Sequence();
        mySequence.Append(transform.DORotate(charRot[5], .2f))
                  .Join(transform.DOMove(charPos[3], 1f))
                  .Insert(1f, transform.DORotate(charRot[4], .2f))
                  .Append(transform.DOMove(charPos[6], 1f))
                  .Append(transform.DORotate(charRot[3], .1f)
                  .SetAutoKill(true).OnComplete(() => seqTwoFinished = true)).OnComplete(() => TriggerAnimation(CAR_ENTRY));
        yield return new WaitUntil(() => seqTwoFinished == true);
        seqTwoFinished = false;
        yield return new WaitForSeconds(2f);
        
        TriggerAnimation(CAR_IDLE);
        Sequence nextSequence = DOTween.Sequence();
        nextSequence.Append(transform.DOMove(charPos[7], moveDuration))
                    .Append(transform.DOMove(charPos[8], moveDuration))
                    .Join(transform.DORotate(charRot[1], rotDuration))
                    .Append(transform.DOMove(charPos[9], moveDuration))
                    .SetAutoKill(true).OnComplete(() => charEntered = true); 



            




    }



     
}
