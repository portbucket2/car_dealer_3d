﻿using TMPro;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using DG.Tweening;
using System.Collections.Generic;
using com.faithstudio.Gameplay;

public class UIManager : BaseCarDealerBehaviour
{
    [Header("Start Panel")]
    public Button tapButton;
    public Animator startPanelAnimator;
    public TextMeshProUGUI levelNumberText;
    public TextMeshProUGUI dayCountText;
    public LevelData currentlvlData;

    [SerializeField]
    private ParticleSystem dollar;
    public Button shopButton;
     
    
    private int ENTRY = Animator.StringToHash("entry");
    private int EXIT = Animator.StringToHash("exit");
    private int SWIPEON = Animator.StringToHash("swipeOn");
    private int SWIPEOFF = Animator.StringToHash("swipeOff");
    private static UIManager m_Instance;

    private WaitForSeconds WAITTWO = new WaitForSeconds(2f);

    [Header("Swipe panel")]

   
    public RectTransform hand;
    public Image right;
    public Image left;
    public Image up;
    public Image stolen;
    public TextMeshProUGUI swipeRight;
    public TextMeshProUGUI swipeLeft;
    public TextMeshProUGUI swipeUp;
    public GameObject swipePanel;

    

    Sequence swipeSequence ;


    [Header("Display Info")]
    
    public TextMeshProUGUI carType;
    public TextMeshProUGUI estPrice;
    public TextMeshProUGUI askPrice;
    public TextMeshProUGUI buyerCar;
    public TextMeshProUGUI buyerPrice;
    //public Slider bodySlider;
    //public Slider performanceSlider;
    public GameObject carDis;
    public GameObject buyerDis;

    public List<Image> performanceImages;
    public List<Image> bodyImages;

    [Header("Sold")]
    public GameObject sold;
    public GameObject notEnough;

    [Header("Action Panel")]
    
    public GameObject accept;
    public GameObject reject;

    [Header("Level Complete")] 
    public Animator levelCompleteAnimator;
    public Button crossButton;

    [Header("Switching Things here")] 
    public GameObject daycountGO;


    private Sequence thiefSequence;

    private bool isAlreadySwiped;


    [Header("Busted panel")] public GameObject bustedPanel;
    public Button bustedCollectButton;
    public bool isBustedRewardCollected;
    
    
    public static UIManager Instance
    {
        get { return m_Instance; }
    }

    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
        }
    }

    private void Start()
    {
        notEnough.SetActive(false);
        UpdateLevelText();
        StartCoroutine(ButtonInteraction());
        stolen.enabled = false;
    }

    public void UpdateLevelText()
    {
        levelNumberText.text =
            "Lvl: "+GameManagerGM.Instance.levelNumber.ToString(); //LevelManager.Instance.GetCurrentLevelWithLevelText();

        dayCountText.text = "Day " + GameManagerGM.Instance.dayCount.ToString();
    }
    

    IEnumerator ButtonInteraction()
    {
        yield return new WaitForEndOfFrame();
       tapButton.onClick.AddListener(delegate
       {
           startPanelAnimator.SetTrigger(EXIT);
           GameManagerGM.Instance.ChangeGameState(GameState.IDLE);
           //ActiveShop();
       });

        shopButton.onClick.AddListener(delegate {
            GameManagerGM.Instance.LoadData();
            
            CDGameplay.Instance.SwitchToWorkshop();
            SlidingController.Instance.InstantiateCar(GameManagerGM.Instance.GetAllSavedCarasAList());
            //DeactivateShop();
        });
            
    }

    public IEnumerator BustedPanelAppear()
    {
        yield return new WaitForSeconds(2.5f);
        bustedPanel.SetActive(true);
        
        bustedCollectButton.onClick.AddListener(delegate
        {
            bustedPanel.SetActive(false);
            isBustedRewardCollected = true;
            dollar.Play();

            BasicCoinTracker._coinMan.ChangeBy(CurrencyType.COIN, 500);
        });
    }

    public void PlayDollar()
    {
        dollar.Play();
    }
    public void ActiveShop()
    {
        shopButton.gameObject.SetActive(true);
    }
    public void DeactivateShop()
    {
        shopButton.gameObject.SetActive(false);
    }

    public void DisplayInfo()
    {
        LevelData levelData = GameManagerGM.Instance.GetLevelData(LevelManager.Instance.GetCurrentLevel());
        currentlvlData = levelData;
        carType.text = levelData.carCategory.ToString();
        estPrice.text = "$" + levelData.carProfile.currentPrice.ToString();
        askPrice.text = "$" + levelData.carProfile.askingPrice.ToString();

        for(int i = 0; i< levelData.carProfile.bodyMeter; i++)
        {
            bodyImages[i].gameObject.SetActive(true);
        }
        for (int i = 0; i < levelData.carProfile.performanceMeter; i++)
        {
            performanceImages[i].gameObject.SetActive(true);
        }

        //bodySlider.value = levelData.carProfile.bodyMeter;
        //performanceSlider.value = levelData.carProfile.performanceMeter;
        carDis.SetActive(true);
        if(CDGameplay.Instance.isThief)
        {
            stolen.enabled = true;
            Stolen();
        }
        else
        {
            stolen.enabled = false;
        }
        SwipeAnimation();


    }

    public void ResetMeter()
    {
        foreach(Image image in bodyImages)
        {
            image.gameObject.SetActive(false);
        }

        foreach(Image image in performanceImages)
        {
            image.gameObject.SetActive(false);
        }
    }

    public void DisplayBuyerInfo()
    {
        LevelData levelData = GameManagerGM.Instance.GetLevelData(LevelManager.Instance.GetCurrentLevel());
        currentlvlData = levelData;
        buyerCar.text = levelData.carCategory.ToString();
        
        buyerDis.SetActive(true);
        SwipeAnimation();
    }

    public void AcceptPanel()
    {

        StartCoroutine(Accept());
    }

    IEnumerator Accept()
    {
        accept.SetActive(true);
        carDis.SetActive(false);

        yield return new WaitForSeconds(2f);
        accept.SetActive(false);
    }

    public void RejectPanel()
    {
        StartCoroutine(Reject());
    }
    IEnumerator Reject()
    {
        reject.SetActive(true);
        carDis.SetActive(false);

        yield return new WaitForSeconds(2f);
        reject.SetActive(false);
    }


    public override void OnIdle(GameplayData gameplayData)
    {
    }
    
    public override void OnCustomerItemDealingStart()
    {
//        Debug.Log("On Item Dealing Start - UI manager");
        StartCoroutine(OnItemDealingStartRoutine());
    }
    private IEnumerator OnItemDealingStartRoutine()
    {
        if(CDGameplay.Instance.isBuyer)
        {
            swipePanel.SetActive(true);
            DisplayBuyerInfo();
        }
        else
        {
            swipePanel.SetActive(true);
            DisplayInfo();
        }
        
        
        
        InputManager.instance.onSwipe += delegate(Dir dir)
        {
            if(!isAlreadySwiped)
            {
                if(dir == Dir.LEFT || dir == Dir.RIGHT || (dir == Dir.UP && CDGameplay.Instance.isThief))
                {
                    isAlreadySwiped = true;
                    Swipe(dir);
                }
                
                
            }
            
        };
        yield return new WaitForEndOfFrame();
        
    }
    private void Swipe(Dir dir)
    {
        Debug.Log(dir + " ---- in UIMANAGER");
        switch (dir)
        {

            case Dir.UP :
                if(CDGameplay.Instance.isThief)
                {
                    CDGameplay.Instance.characterPrefab.GetComponent<CharController>().ThiefUpSwipeBehavior();
                    PoliceController.instance.PoliceBehavior();
                    CDGameplay.Instance.carPrefab.GetComponent<CarEntry>().CarReject();
                    
                    StopSwipe();
                    StopUpSwipe();
                    RejectPanel();
                    ResetMeter();
                    LevelManager.Instance.IncreaseGameLevel();
                    
                   StartCoroutine( BustedPanelAppear());
                }
                
                break;
            case Dir.DOWN:
                break;
            case Dir.LEFT:
                if(CDGameplay.Instance.isBuyer)
                {
                    CDGameplay.Instance.characterPrefab.GetComponent<CharController>().AcceptCharacter();
                    buyerDis.SetActive(false);
                   
                }
                else
                {
                    CDGameplay.Instance.carPrefab.GetComponent<CarEntry>().ExitSequence();
                    CDGameplay.Instance.characterPrefab.GetComponent<CharController>().ExitCharacter();
                    GameManagerGM.Instance.SaveData();
                }
                
                ResetMeter();
                RejectPanel();
                StopSwipe();
                StopUpSwipe();
               
                LevelManager.Instance.IncreaseGameLevel();

                break;
            case Dir.RIGHT:
                if(BasicCoinTracker.coinMan.GetBalance(CurrencyType.COIN) >= currentlvlData.carProfile.askingPrice || CDGameplay.Instance.isBuyer)
                {
                    if (CDGameplay.Instance.isBuyer)
                    {
                        //CDGameplay.Instance.characterPrefab.GetComponent<CharController>().ExitCharacter();
                        buyerDis.SetActive(false);
                    }
                    else
                    {
                        CDGameplay.Instance.carPrefab.GetComponent<CarEntry>().CarAccept();
                        CDGameplay.Instance.characterPrefab.GetComponent<CharController>().AcceptCharacter();
                        BasicCoinTracker.coinMan.ChangeBy(CurrencyType.COIN, -currentlvlData.carProfile.askingPrice);
                        AcceptPanel();
                    }
                    StopUpSwipe();

                    ResetMeter();

                    StopSwipe();
                    StartCoroutine(SwitchToShop());
                    //DeactivateShop();
                }
                else
                {
                    notEnough.SetActive(true);
                    CrossButtonInteraction();
                    Debug.Log(currentlvlData.carProfile.askingPrice);
                }
                
                break;
            
            default:
                break;
        }
    }

    private void CrossButtonInteraction()
    {
        crossButton.onClick.AddListener(delegate
        {
            CDGameplay.Instance.carPrefab.GetComponent<CarEntry>().ExitSequence();
            CDGameplay.Instance.characterPrefab.GetComponent<CharController>().ExitCharacter();
            ResetMeter();
            RejectPanel();
            StopSwipe();
            StopUpSwipe();
            notEnough.SetActive(false);
            LevelManager.Instance.IncreaseGameLevel();
            
        });
    }

    public IEnumerator SwitchToShop()
    {
        //CDGameplay.Instance.AddCartoInventory();
        if (!CDGameplay.Instance.isBuyer)
        {
            CDGameplay.Instance.SaveCarWhileBackFromWorkshop();
        }

        yield return new WaitForSeconds(3f);
        daycountGO.SetActive(false);
        buyerDis.SetActive(false);
        swipePanel.SetActive(false);
        
        SlidingController.Instance.InstantiateCar(GameManagerGM.Instance.GetAllSavedCarasAList());
        CDGameplay.Instance.SwitchToWorkshop();
        GameManagerGM.Instance.SaveData();
        LevelManager.Instance.IncreaseGameLevel();
    }

    

    public void ResetLevel()
    {
        CarEntry.instance.carEntered = false;
        CarEntry.instance.carAccepted = false;
        CarEntry.instance.carExited = false;

        CharController.instance.charEntered = false;
        CharController.instance.charExited = false;
        CharController.instance.charAccepted = false;
        isAlreadySwiped = false;
        
        UpdateLevelText();

        //up.enabled = false;
    }
    

    public void SwipeAnimation()
    {
        //Debug.LogError(CDGameplay.Instance.isThief);
        if(CDGameplay.Instance.isThief)
        {
            StartCoroutine(SwipeThief());
        }
        else
        {
            StartCoroutine(SwipeAnima());
        }
    }

    IEnumerator SwipeAnima()
    {
        yield return new WaitForEndOfFrame();
        swipeSequence = DOTween.Sequence();
        swipeSequence.OnStart(() => StartSetup())
                     
                     .Append(hand.DOAnchorPosX(400, 1.5f))
                     .Join(swipeRight.rectTransform.DOScale(new Vector3(1f, 1f, 1), 1.5f).SetEase(Ease.OutExpo))
                     .Join(right.DOFillAmount(1, 1.5f))
                     
                     .Append(hand.DOAnchorPosX(0, 0))
                     .Join(right.DOFillAmount(0, 0f))
                     .Join(swipeRight.rectTransform.DOScale(new Vector3(0f, 0f, 0), 0f))

                     .Append(hand.DOAnchorPosX(-400, 1.5f))
                     .Join(swipeLeft.rectTransform.DOScale(new Vector3(1f, 1f, 1), 1.5f).SetEase(Ease.OutExpo))
                     .Join(left.DOFillAmount(1, 1.5f))
                     .AppendCallback(()=> swipeSequence.Restart());


        
    }

    IEnumerator SwipeThief()
    {
        yield return new WaitForEndOfFrame();
        thiefSequence = DOTween.Sequence();
        thiefSequence.OnStart(()=> StartSetup())
                     .Append(hand.DOAnchorPosX(400, 1.5f))
                     .Join(swipeRight.rectTransform.DOScale(new Vector3(1f, 1f, 1), 1.5f).SetEase(Ease.OutExpo))
                     .Join(right.DOFillAmount(1, 1.5f))

                     .Append(hand.DOAnchorPosX(0, 0))
                     .Join(right.DOFillAmount(0, 0f))
                     .Join(swipeRight.rectTransform.DOScale(new Vector3(0f, 0f, 0), 0f))

                     .Append(hand.DOAnchorPosX(-400, 1.5f))
                     .Join(swipeLeft.rectTransform.DOScale(new Vector3(1f, 1f, 1), 1.5f).SetEase(Ease.OutExpo))
                     .Join(left.DOFillAmount(1, 1.5f))

                     .Append(hand.DOAnchorPosX(0, 0))
                     .Append(left.DOFillAmount(0,0f))
                     .Append(swipeLeft.rectTransform.DOScale(new Vector3(0f, 0f, 0), 0f))

                     .Append(hand.DOAnchorPosY(-300, 1.5f))
                     .Join(up.DOFillAmount(1, 1.5f))
                     .Join(swipeUp.rectTransform.DOScale(new Vector3(1f, 1f, 1), 1.5f).SetEase(Ease.OutExpo))
                     .AppendCallback(() => thiefSequence.Restart());
    }
    void StartSetup()
    {
        hand.anchoredPosition = new Vector3(0, -550, 0);
        left.fillAmount = 0;
        right.fillAmount = 0;
        up.fillAmount = 0;
        swipeRight.rectTransform.localScale = new Vector3(0, 0, 0);
        swipeLeft.rectTransform.localScale = new Vector3(0, 0, 0);
        swipeUp.rectTransform.localScale = new Vector3(0, 0, 0);
    }
    
    public void Stolen()
    {
        stolen.transform.DOScale(new Vector3(1.5f, 1.5f, 0), .5f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutFlash);
    }
    

    public void StopSwipe()
    {
        
        
        swipeSequence.Kill();
        swipeSequence.SetLoops(0);
        swipeSequence.Complete();
        swipePanel.SetActive(false);

    }

    public void StopUpSwipe()
    {
        
        
        thiefSequence.Kill();
        thiefSequence.SetLoops(0);
        thiefSequence.Complete();
        swipePanel.SetActive(false);

    }

    public void ActivateShowroom()
    {
        daycountGO.SetActive(true);

        //BuyerCarIns();
    }

    public void BuyerCarIns()
    {
        StartCoroutine(BuyerOnly());
        BasicCoinTracker.coinMan.ChangeBy(CurrencyType.COIN, +SlidingController.Instance.GetCarProfile().GetRestoredValue());
        dollar.Play();
    }

    IEnumerator BuyerOnly()
    {
        if (CDGameplay.Instance.isBuyer)
        {
            CDGameplay.Instance.SwitchToShowRoom();
            GameObject carForBuyer = Instantiate(CDGameplay.Instance.carForBuyer.cleanCarPrefab, new Vector3(0, -0.46f, 8.55f), Quaternion.Euler(0, 180, 0));
            CDGameplay.Instance.characterPrefab.GetComponent<CharController>().ExitCharacter();
            carForBuyer.GetComponent<CarEntry>().ExitSequence();
            AcceptPanel();
            yield return new WaitUntil(() => carForBuyer.GetComponent<CarEntry>().carExited == true);
            Destroy(carForBuyer);
            
            GameManagerGM.Instance.RemoveCarFromSaved(SlidingController.Instance.GetCarProfile(),SlidingController.Instance.GetSelectedCarLevelNumber());
            GameManagerGM.Instance.SaveData();
            GameManagerGM.Instance.LoadData();
            
            SlidingController.Instance.ResetCarHolder();
        }
    }
    
    
}
