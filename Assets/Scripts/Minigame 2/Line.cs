﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line : MonoBehaviour
{

    private LineRenderer lineRenderer;
    private RectTransform rectTransform;
    private Vector2 tapPos;
    private Vector2 startTapPos;

    

    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        this.SetLine();
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            startTapPos = rectTransform.anchoredPosition;
        }
        if(Input.GetMouseButton(0))
        {
            tapPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            lineRenderer.SetPosition(0, new Vector3(startTapPos.x, startTapPos.y, 0f));
            lineRenderer.SetPosition(1, new Vector3(tapPos.x, tapPos.y, 0f));
        }
    }
    
    private void SetLine()
    {
        GameObject lineObject = new GameObject();
        this.lineRenderer = lineObject.AddComponent<LineRenderer>();
        this.lineRenderer.startWidth = 3f;
        this.lineRenderer.endWidth = 3f;
        

    }

}
