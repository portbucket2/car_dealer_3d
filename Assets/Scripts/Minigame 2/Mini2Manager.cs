﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mini2Manager : MonoBehaviour
{
    public int done = 0;
    public Canvas canvas;
    public static Mini2Manager instance;


    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    private void Update()
    {
        if(done >= 3)
        {
            canvas.gameObject.SetActive(true);
        }
    }
}
