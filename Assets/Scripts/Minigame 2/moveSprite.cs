﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveSprite : MonoBehaviour
{

    private Vector2 startPos;
    private Vector2 mousePosition;
    [SerializeField]
    private Transform snap;
    [SerializeField]
    private Transform placeholder;
    private float deltaX, deltaY;
    public static bool locked;
    public Camera camera;
    public string sortingLayerName;
    public int orderInLayer;
    private LineRenderer lineRenderer;
    private Transform draggable;
    public Shader unlit;

    [SerializeField]
    private SpriteRenderer indicator;
    public SpriteRenderer lightup;

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;

        SetLine();
        
    }

    // Update is called once per frame
    private void OnMouseDown()
    {
        if(!locked)
        {
            deltaX = camera.ScreenToWorldPoint(Input.mousePosition).x - transform.position.x;
            
            deltaY = camera.ScreenToWorldPoint(Input.mousePosition).y - transform.position.y;
            lineRenderer.SetPosition(0,placeholder.position);
        }

        
    }


    private void OnMouseDrag()
    {
        if(!locked)
        {
            mousePosition = camera.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector2(mousePosition.x - deltaX, mousePosition.y - deltaY);
            
            lineRenderer.SetPosition(1, transform.position);

        }
    }

    private void OnMouseUp()
    {
        if (Mathf.Abs(transform.position.x - snap.position.x) <= 0.5f &&
            Mathf.Abs(transform.position.y - snap.position.y) <= 0.5f)
        {
            transform.position = new Vector2(snap.position.x, snap.position.y);
            lineRenderer.SetPosition(1, transform.position);
            lightup.color = indicator.color;
            Mini2Manager.instance.done++;
        }

        else
        {
            transform.position = new Vector2(startPos.x, startPos.y);
            lineRenderer.SetPosition(1, placeholder.position);
        }
    }

    private void SetLine()
    {
        GameObject lineObject = new GameObject();
        this.lineRenderer = lineObject.AddComponent<LineRenderer>();
        this.lineRenderer.startWidth = 1f;
        this.lineRenderer.endWidth = 1f;
        this.lineRenderer.sortingLayerName = sortingLayerName;
        this.lineRenderer.sortingOrder = orderInLayer;
        this.lineRenderer.material.color = snap.GetComponent<SpriteRenderer>().color;
        this.lineRenderer.material.shader = unlit;


    }

    

}
