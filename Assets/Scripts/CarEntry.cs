﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


[System.Serializable]
public struct ScaleData
{
    public float size;
    public float time;
}
public class CarEntry : MonoBehaviour
{
    //Moving the cars to the desired points
    [SerializeField]
    private Ease linear;
    [SerializeField]
    private Ease inSine;

    public static CarEntry m_Instance;

    //Scaling the car to mimic bad engine shaking

    [SerializeField]
    private ScaleData[] scaleDatas;
    private int index;
    [SerializeField]
    private float moveDuration;
    [SerializeField]
    private float rotDuration;

    [SerializeField]
    private Vector3[] carPos;
    [SerializeField]
    private Vector3[] carRot;

    [SerializeField]
    private Transform door;

    public bool carEntered;
    public bool carExited;
    public bool carAccepted;
    public bool carRejected;

    public static CarEntry instance
    {
        get { return m_Instance; }
    }



    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
        }
    }

    void Start()
    {
        ScaleCar();
        carEntered = false;
        carExited = false;
        carAccepted = false;
        carRejected = false;
        //EntrySequence();
    }



    public void ScaleCar()
    {

        if(index >= scaleDatas.Length)
        {
            index = 0;
        }
        transform.DOScaleY(scaleDatas[index].size, scaleDatas[index].time).OnComplete(() => ScaleCar());
        index++;
    }

    public void EntrySequence()
    {
        Sequence mySequence = DOTween.Sequence();

        mySequence.Append(transform.DOMove(carPos[0], moveDuration))
                  .Append(transform.DOMove(carPos[1], moveDuration))
                  .Join(transform.DORotate(carRot[0], rotDuration).SetEase(inSine))
                  .PrependInterval(1)
                  .Append(transform.DOMove(carPos[2], moveDuration).SetEase(inSine))
                  .Insert(3.5f, door.transform.DORotate(carRot[3], .5f))
                  .Insert(5f, door.transform.DORotate(carRot[4], .5f))
                  .SetAutoKill(true).OnComplete(() => carEntered = true);
        
    }

    IEnumerator ExitCar()
    {

        yield return new WaitForSeconds(3.8f);
        Sequence exitSequence = DOTween.Sequence();
        carRejected = true;
        exitSequence.Insert(0, door.transform.DORotate(carRot[3], .5f))
                    .Insert(1f, door.transform.DORotate(carRot[4], .5f))
                    .Append(transform.DOMove(carPos[5], moveDuration))
                    
                    .Append(transform.DOMove(carPos[4], moveDuration))
                    .Join(transform.DORotate(carRot[1], rotDuration).SetEase(linear))
                    .PrependInterval(1)
                    .Append(transform.DOMove(carPos[3], .5f).SetEase(inSine)).OnComplete(() => carExited = true)
                    .SetAutoKill(true);
    }
    public void ExitSequence()
    {

        StartCoroutine(ExitCar());
    }

    public void CarAccept()
    {
        Sequence acceptSequence = DOTween.Sequence();
        carAccepted = true;
        acceptSequence.Append(transform.DOMove(carPos[6], moveDuration))
                      .Join(transform.DORotate(carRot[1], rotDuration))
                      .PrependInterval(1)
                      .Append(transform.DOMove(carPos[7], moveDuration).SetEase(inSine)).OnComplete(()=> carAccepted = true);
                        
    }

    public void CarReject()
    {
        Sequence rejectSequence = DOTween.Sequence();

        rejectSequence.Append(transform.DOMove(carPos[8], moveDuration))
                      .Join(transform.DORotate(carRot[2], rotDuration))
                      .PrependInterval(1)
                      .Append(transform.DOMove(carPos[9], moveDuration).SetEase(inSine)).OnComplete(()=> carRejected = true);
    }

    
    
}
