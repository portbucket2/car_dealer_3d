﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CDGameplay : BaseCarDealerBehaviour
{
    public static CDGameplay Instance;

    public GameObject inputManager;
    public int currentGameLevel;
    private LevelData m_CurrentLevelData;

    public GameObject carPrefab;

    public CarAssetClass carForBuyer;
    
    public GameObject characterPrefab;
    public bool isBuyer;
    public bool isThief;
    CarEntry carEnter;
    CharController characterController;
    
    
    [Header("Switch Cam")]
    [Header("Shop Things Here")] 
    public GameObject showroomCam;
    public GameObject workshopCam;


    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }
    public override void OnNone()
    {
        StartCoroutine(OnNoneRoutine());
    }

    public override void OnIdle(GameplayData gameplayData)
    {
        StartCoroutine(OnIdleRoutine());
    }

    public override void OnLevelInitializing(LevelInitializingData levelInitializingData)
    {
        StartCoroutine(OnLevelInitializingRoutine());
    }

    public override void OnLevelStarting()
    {
        StartCoroutine(OnLevelStartingRoutine());
    }

    public override void OnCustomerArriving()
    {
        StartCoroutine(OnCustomerArrivingRoutine());
    }

    public override void OnCustomerItemScanning()
    {
        StartCoroutine(OnItemScanningRoutine());
    }

    public override void OnCustomerItemDealingStart()
    {
        StartCoroutine(OnItemDealingStartRoutine());
    }

    public override void OnCustomerItemDealingEnd(CustomerInputType customerInputType)
    {
        StartCoroutine(OnItemDealingEndRoutine());
    }

    public override void OnCustomerLeavingShop()
    {
        StartCoroutine(OnCustomerLeavingRoutine());
    }

    public override void OnLevelEnding(GameplayData gameplayData)
    {
        StartCoroutine(OnLevelEndingRoutine());
    }

    private IEnumerator OnNoneRoutine()
    {
        yield break ;
    }
    private IEnumerator OnIdleRoutine()
    {
        currentGameLevel = LevelManager.Instance.GetCurrentLevel();
        
        //Debug.LogWarning(currentGameLevel + " THIS IS FROM IDLE");
        if(currentGameLevel> 5)
        {
            currentGameLevel = 1;
        }
        
        if (GameManagerGM.Instance.levelNumber != 1 && GameManagerGM.Instance.levelNumber % 5 == 1)
        {
            GameManagerGM.Instance.ReadCSVAgain();
        }
        
        
        LionKitManager.Instance.LevelStarted(GameManagerGM.Instance.levelNumber);
        
       
       //Debug.LogError("CURRENT LEVEL -- " +currentGameLevel);
       
        isBuyer = false;
        isThief = false;
        GameManagerGM.Instance.ChangeGameState(GameState.LEVEL_INITIALIZING);
        yield return new WaitForEndOfFrame();
    }
    private IEnumerator OnLevelInitializingRoutine()
    {
        
        m_CurrentLevelData = GameManagerGM.Instance.GetLevelData(currentGameLevel);
        //Instantiate all your necessary Car with Level Data,
        //Character and car here for this level

        if(m_CurrentLevelData.customerMode == CustomerMode.SELLER)
        {
            CarAssetClass car = AssetContainer.Instance.GetCarWithCategory(m_CurrentLevelData.carCategory);
            carPrefab = Instantiate(car.brokenCarPrefab);
            carEnter = carPrefab.GetComponent<CarEntry>();

            CharacterAssetClass character = AssetContainer.Instance.GetCharacterWithNumber(currentGameLevel);
            characterPrefab = Instantiate(character.customerPrefab) as GameObject;
            characterPrefab.GetComponent<CharController>().charAnim = characterPrefab.GetComponent<Animator>();
            characterController = characterPrefab.GetComponent<CharController>();
        }
        else if(m_CurrentLevelData.customerMode == CustomerMode.BUYER)
        {
            isBuyer = true;

            CarAssetClass car = AssetContainer.Instance.GetCarWithCategory(m_CurrentLevelData.carCategory);
            carForBuyer = car;

            CharacterAssetClass character = AssetContainer.Instance.GetCharacterWithNumber(currentGameLevel);
            characterPrefab = Instantiate(character.customerPrefab) as GameObject;
            characterPrefab.GetComponent<CharController>().charAnim = characterPrefab.GetComponent<Animator>();
            characterController = characterPrefab.GetComponent<CharController>();
        }
        
        //Debug.LogError(m_CurrentLevelData.customerType);
        if(m_CurrentLevelData.customerType == CustomerType.THIEF)
        {
            isThief = true;
        }
        
        
        yield return new WaitForEndOfFrame();
        GameManagerGM.Instance.ChangeGameState(GameState.LEVEL_STARTING);
    }
    private IEnumerator OnLevelStartingRoutine()
    {
        //yield return new WaitForSeconds(1f);
        m_CurrentLevelData = GameManagerGM.Instance.GetLevelData(currentGameLevel);

        if(m_CurrentLevelData.customerMode == CustomerMode.SELLER && !isThief)
        {
            carEnter.EntrySequence();
            characterController.EnteyCharacter();
            yield return new WaitUntil(() => characterController.charExited == true && carEnter.carEntered == true);
            GameManagerGM.Instance.ChangeGameState(GameState.CUSTOMER_ARRIVING);
        }
        else if(m_CurrentLevelData.customerMode == CustomerMode.BUYER)
        {
            characterController.BuyerEntry();
            yield return new WaitUntil(() => characterController.charExited == true);
            GameManagerGM.Instance.ChangeGameState(GameState.CUSTOMER_ARRIVING);
        }
        else if(isThief)
        {
            
            carEnter.EntrySequence();
            characterController.ThiefEntry();
            yield return new WaitUntil(() => characterController.charExited == true && carEnter.carEntered == true);
            GameManagerGM.Instance.ChangeGameState(GameState.CUSTOMER_ARRIVING);
        }
        
        
        yield return new WaitForEndOfFrame();
    }
    private IEnumerator OnCustomerArrivingRoutine()
    {
        Debug.LogWarning("State Arrived");
        yield return new WaitForEndOfFrame();
        inputManager.SetActive(true);
        yield return new WaitForEndOfFrame();

        UIManager.Instance.OnCustomerItemDealingStart();
        GameManagerGM.Instance.ChangeGameState(GameState.CUSTOMER_ITEM_SCANNING);
        yield return new WaitForEndOfFrame();
    }
    private IEnumerator OnItemScanningRoutine()
    {
        GameManagerGM.Instance.ChangeGameState(GameState.CUSTOMER_ITEM_DEALING_START);
        yield return new WaitForEndOfFrame();
    }
    private IEnumerator OnItemDealingStartRoutine()
    {
        
        GameManagerGM.Instance.ChangeGameState(GameState.CUSTOMER_ITEM_DEALING_END);

        yield return new WaitForEndOfFrame();
    }
    private IEnumerator OnItemDealingEndRoutine()
    {
        //GameManagerGM.Instance.ChangeGameState(GameState.CUSTOMER_ITEM_DEALING_END);
        GameManagerGM.Instance.ChangeGameState(GameState.CUSTOMER_LEAVING_SHOP);
        yield return new WaitForEndOfFrame();
    }
    private IEnumerator OnCustomerLeavingRoutine()
    {
        
        Debug.Log(PoliceController.instance.isArrest + " ARR ---- "+UIManager.Instance.isBustedRewardCollected);
        yield return new WaitUntil(() => (characterController.charAccepted == true && WorkshopUI.Instance.isShopBack) 
                                         || characterController.charEntered == true || 
                                         (isBuyer && characterController.charAccepted == true)
                                         || (PoliceController.instance.isArrest && UIManager.Instance.isBustedRewardCollected));
        
        Debug.Log("END BEFORE ENBD");
        inputManager.SetActive(false);
        GameManagerGM.Instance.ChangeGameState(GameState.LEVEL_ENDING);
        yield return new WaitForEndOfFrame();
    }
    private IEnumerator OnLevelEndingRoutine()
    {

        Destroy(carPrefab);
        Destroy(characterPrefab);
        //LevelManager.Instance.IncreaseGameLevel();
        StartCoroutine(OnIdleRoutine());
        UIManager.Instance.ResetLevel();
        WorkshopUI.Instance.ShopBackReset();
        UIManager.Instance.gameObject.SetActive(true);
        yield return new WaitForEndOfFrame();
        //isThief = false;
        //isBuyer = false;
        
        //Debug.Log(isThief + " ---- Isthir");
        PoliceController.instance.ResetArrest();
        
        LionKitManager.Instance.LevelCompleted(GameManagerGM.Instance.levelNumber-1);
    }





    public void SwitchToWorkshop()
    {
        showroomCam.SetActive(false);
        workshopCam.SetActive(true);
        WorkshopUI.Instance.ActivateShop();
    }

    public void SwitchToShowRoom()
    {
        
        showroomCam.SetActive(true);
        workshopCam.SetActive(false);
        UIManager.Instance.ActivateShowroom();
    }
    
    public List<CarProfile> tempAddedCarProfile = new List<CarProfile>();

    public void AddCartoInventory()
    {
        tempAddedCarProfile.Add(m_CurrentLevelData.carProfile);

        if (!isBuyer)
        {
            //GameManagerGM.Instance.AddCarToSave(m_CurrentLevelData.carProfile,LevelManager.Instance.GetCurrentLevel());
        }
    }

    public void SaveCarWhileBackFromWorkshop()
    {
        if (!isBuyer && !WorkshopUI.Instance.isQuickSell)
        {
            //m_CurrentLevelData.carProfile.levelNumber = GameManagerGM.Instance.levelNumber;
            Debug.Log(GameManagerGM.Instance.levelNumber + " Level number while saving data");
            GameManagerGM.Instance.AddCarToSave(m_CurrentLevelData.carProfile,GameManagerGM.Instance.levelNumber);
        }
    }

    public CarProfile GetCurrentBoughtCar()
    {
        return m_CurrentLevelData.carProfile;
    }

}
