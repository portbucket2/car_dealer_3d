﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WorkshopUI : MonoBehaviour
{
    [Header("Selected Car Info")] 
    public GameObject infoPanel;
    public TextMeshProUGUI selectedCarType;
    public List<Image> performanceBarList;
    public List<Image> bodyBarList;
    public Button performanceRepairButton;
    public Button bodyRepairButton;
    public Button quickSellButton;
    public TextMeshProUGUI boughtPriceText;
    public TextMeshProUGUI estPriceText;

    [Header("Body Repair UI")] 
    public GameObject bodyRepaiarBarGO;
    public GameObject tapAndHoldGO;
    public List<Image> bodyRepairCompletionBarList;
    
    

    [Header("Podium Button")] public Button rightPodiumButton;
    public Button leftPodiumButton;


    public Button backToShowRoomButton;
    public bool isShopButton;
    public bool isShopBack;

    [Header("Workshop Activator")] 
    public GameObject workshopCanvas;

    public bool isQuickSell;

    [Header("Minigame Segment")]
    public Camera mainCam;
    public GameObject engineMinigameGO;
    public GameObject engineMinigameCode;
    public IMinigame engineMini;

    public List<MinigameSetup> minigameList;

    public GameObject wireGameObject;
    
    
    //------------------//

    private float m_ElapsedTime = 0;
    public bool m_IsBodyRepairCompleted;
    public bool m_IsBodyRepairStarted;
    public bool m_IsRoutineStarted;
    private int m_Counter;

    public static WorkshopUI Instance;

    [Header("Selling Car UI")] 
    public GameObject sellingUIPanel;

    public Image carImage;
    public TextMeshProUGUI carTypeText;
    public TextMeshProUGUI sellText;
    public TextMeshProUGUI boughtText;
    public TextMeshProUGUI profitText;
    public Button sellCancelButton;
    public Button sellPanelButton;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        for (int i = 0; i < minigameList.Count-1; i++)
        {
            minigameList[i].engineMini = minigameList[i].minigameCode.GetComponent<IMinigame>();
        }
    }


    private void Start()
    {
        ButtonInteraction();
    }

    private void ButtonInteraction()
    {
        rightPodiumButton.onClick.AddListener(delegate
        {
            SlidingController.Instance.RightMovement();
            ShowInfoPanel(SlidingController.Instance.GetCarProfile());
        });
        
        leftPodiumButton.onClick.AddListener(delegate
        {
            SlidingController.Instance.LeftMovement();
            ShowInfoPanel(SlidingController.Instance.GetCarProfile());
        });
        
        backToShowRoomButton.onClick.AddListener(delegate
        {
            workshopCanvas.SetActive(false);
            CDGameplay.Instance.SwitchToShowRoom();
            isShopBack = true;
            SlidingController.Instance.ResetCarHolder();
            CDGameplay.Instance.characterPrefab.GetComponent<CharController>().AcceptCharacter();
            if(CDGameplay.Instance.isBuyer)
            {
                UIManager.Instance.RejectPanel();
            }
            //CDGameplay.Instance.SaveCarWhileBackFromWorkshop();
            isQuickSell = false;
            //UIManager.Instance.ActiveShop();
        });
    }
    

    private void Update()
    {
        if (m_IsBodyRepairStarted)
        {
            if (Input.GetMouseButtonDown(0))
            {
                tapAndHoldGO.SetActive(false);
            }

            if (Input.GetMouseButton(0))
            {
                //Tap and Hold
                if (!m_IsBodyRepairCompleted)
                {
                    m_ElapsedTime += Time.deltaTime;

                    if (m_ElapsedTime > 2.5f)
                    {
                        m_Counter++;
                        FillCompletionBar(m_Counter);
                        m_ElapsedTime = 0;
                        if (m_Counter == 5)
                        {
                            m_IsBodyRepairCompleted = true;
                        }
                    }

                    else if (m_ElapsedTime > 0.5f && !m_IsRoutineStarted)
                    {
                        WorkshopController.Instance.StartBodyRepair();
                        m_IsRoutineStarted = true;
                    }

                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                m_ElapsedTime = 0;
                if (!m_IsBodyRepairCompleted)
                {
                    ResetBodyRepairCompletionBar();
                    WorkshopController.Instance.StopBodyRepair();
                    m_IsRoutineStarted = false;
                    m_Counter = 0;
                }

                if (m_IsBodyRepairCompleted)
                {
                    tapAndHoldGO.SetActive(false);
                    bodyRepaiarBarGO.SetActive(false);
                    
                    CarProfile t_CarProfile = SlidingController.Instance.GetCarProfile();
                    t_CarProfile.IncreaseBodyMeter(5);
                    t_CarProfile.isPolished = true;
                    
                    
                    ShowInfoPanel(t_CarProfile);
                    
                    Debug.LogWarning("Fuck it here. ");
                    GameManagerGM.Instance.AddCarToSave(t_CarProfile,t_CarProfile.levelNumber);
                    GameManagerGM.Instance.SaveData();
                    
                    infoPanel.SetActive(true);
                    leftPodiumButton.gameObject.SetActive(true);
                    rightPodiumButton.gameObject.SetActive(true);
                    backToShowRoomButton.gameObject.SetActive(true);
                    m_ElapsedTime = 0;
                    m_IsBodyRepairCompleted = false;
                    m_IsBodyRepairStarted = false;
                    m_IsRoutineStarted = false;
                    m_Counter = 0;
                }
                else
                {
                    tapAndHoldGO.SetActive(true);
                }
            }
        }
    }


    public void StartBodyRepair()
    {
        StartCoroutine(WaitAndStartBodyRepair());
    }

    IEnumerator WaitAndStartBodyRepair()
    {
        bodyRepaiarBarGO.SetActive(true);
        tapAndHoldGO.SetActive(true);
        ResetBodyRepairCompletionBar();
        
        infoPanel.SetActive(false);
        leftPodiumButton.gameObject.SetActive(false);
        rightPodiumButton.gameObject.SetActive(false);
        backToShowRoomButton.gameObject.SetActive(false);
        
        yield return new WaitForSeconds(0.1f);
        m_IsBodyRepairStarted = true;
    }

    public void StartPerformanceRepair()
    {
        StartCoroutine(WaitAndStartPerformanceRepair());
    }

    private IEnumerator WaitAndStartPerformanceRepair()
    {
        infoPanel.SetActive(false);
        leftPodiumButton.gameObject.SetActive(false);
        rightPodiumButton.gameObject.SetActive(false);
        backToShowRoomButton.gameObject.SetActive(false);
        
        yield return new WaitForSeconds(0.1f);
        
        mainCam.gameObject.SetActive(false);
        //engineMinigameGO.SetActive(true);

        //engineMini.OnMinigameEnd += EngineMinigameEnd;

        CarCategory carID = SlidingController.Instance.GetCarProfile().carCategory;
        
        Debug.LogError(carID + " CAR ID ");
        switch (carID)
        {
            case CarCategory.SEDAN:
                minigameList[0].minigameGO.SetActive(true);
                minigameList[0].minigameCam.SetActive(true);
                minigameList[0].engineMini.Reset();
                minigameList[0].engineMini.ActivateMinigame();
                minigameList[0].engineMini.OnMinigameEnd += EngineMinigameEnd;
                break;
            case CarCategory.MUSCLE:
               
                minigameList[1].minigameGO.SetActive(true);
                minigameList[1].minigameCam.SetActive(true);
                minigameList[1].engineMini.Reset();
                minigameList[1].engineMini.OnMinigameEnd += GearMinigameEnd;
                break;
                
               
            case CarCategory.CONVERTIBLE:
                wireGameObject = Instantiate(minigameList[2].prefab,minigameList[2].minigameGO.transform);
                
                wireGameObject.transform.localPosition = Vector3.zero;
                
                minigameList[2].minigameCode = wireGameObject.transform.GetChild(3).gameObject;
                minigameList[2].engineMini = minigameList[1].minigameCode.GetComponent<IMinigame>();
                
                minigameList[2].minigameGO.SetActive(true);
                wireGameObject.SetActive(true);
                minigameList[2].minigameCam.SetActive(true);
                
                wireGameObject.transform.GetChild(3).gameObject.GetComponent<WireManager>().Set(this);
                
               
                minigameList[2].engineMini.OnMinigameEnd += WireMinigameEnd;
                break;
        }

    }
    private void GearMinigameEnd()
    {
        Debug.Log("gear should falsed here");
        minigameList[1].minigameGO.SetActive(false);
        minigameList[1].minigameCam.SetActive(false);
        infoPanel.SetActive(true);

        CarProfile t_CarProfile = SlidingController.Instance.GetCarProfile();
        t_CarProfile.IncreasePerformanceMeter(5);
        t_CarProfile.isPerformanceIncrease = true;
        ShowInfoPanel(t_CarProfile);
        
        GameManagerGM.Instance.AddCarToSave(t_CarProfile,t_CarProfile.levelNumber);
        GameManagerGM.Instance.SaveData();
        
        leftPodiumButton.gameObject.SetActive(true);
        rightPodiumButton.gameObject.SetActive(true);
        mainCam.gameObject.SetActive(true);
        backToShowRoomButton.gameObject.SetActive(true);
        
        
    }
    public void WireMinigameEnd()
    {
        Debug.Log("WIRE MINIGAME END");
        minigameList[2].minigameGO.SetActive(false);
        minigameList[2].minigameCam.SetActive(false);
        infoPanel.SetActive(true);

        CarProfile t_CarProfile = SlidingController.Instance.GetCarProfile();
        t_CarProfile.IncreasePerformanceMeter(5);
        t_CarProfile.isPerformanceIncrease = true;
        ShowInfoPanel(t_CarProfile);
        
        GameManagerGM.Instance.AddCarToSave(t_CarProfile,t_CarProfile.levelNumber);
        GameManagerGM.Instance.SaveData();
        
        leftPodiumButton.gameObject.SetActive(true);
        rightPodiumButton.gameObject.SetActive(true);
        mainCam.gameObject.SetActive(true);
        backToShowRoomButton.gameObject.SetActive(true);
        Destroy(wireGameObject);
    }

    private void EngineMinigameEnd()
    {
        minigameList[0].minigameGO.SetActive(false);
        minigameList[0].minigameCam.SetActive(false);
        
        infoPanel.SetActive(true);

        CarProfile t_CarProfile = SlidingController.Instance.GetCarProfile();
        t_CarProfile.IncreasePerformanceMeter(5);
        t_CarProfile.isPerformanceIncrease = true;
        ShowInfoPanel(t_CarProfile);
        
        GameManagerGM.Instance.AddCarToSave(t_CarProfile,t_CarProfile.levelNumber);
        GameManagerGM.Instance.SaveData();
        
        leftPodiumButton.gameObject.SetActive(true);
        rightPodiumButton.gameObject.SetActive(true);
        mainCam.gameObject.SetActive(true);
        backToShowRoomButton.gameObject.SetActive(true);
    }

    private void FillCompletionBar(int t_Value)
    {
        if (t_Value <= bodyRepairCompletionBarList.Count)
        {
            bodyRepairCompletionBarList[t_Value-1].enabled = true;
        }
        else
        {
            Debug.Log("BAR FILL UP - REPAIR DONE");
            m_IsBodyRepairCompleted = true;
        }
    }

    public void ShowInfoPanel(CarProfile t_CarProfile)
    {
        infoPanel.SetActive(true);
        leftPodiumButton.gameObject.SetActive(true);
        rightPodiumButton.gameObject.SetActive(true);
        
        selectedCarType.text = t_CarProfile.carCategory.ToString();
        boughtPriceText.text = "$"+t_CarProfile.askingPrice.ToString();
        estPriceText.text = "$" + t_CarProfile.GetRestoredValue();
        
        
        //Debug.LogError(t_CarProfile.bodyMeter);
        SetPerformanceBar(t_CarProfile.performanceMeter);
        SetBodyBar(t_CarProfile.bodyMeter);
        
        performanceRepairButton.onClick.RemoveAllListeners();
        performanceRepairButton.onClick.AddListener(delegate
        {
            StartPerformanceRepair();
        });
        
        bodyRepairButton.onClick.RemoveAllListeners();
        bodyRepairButton.onClick.AddListener(delegate
        {
            StartBodyRepair();
        });

        if (CDGameplay.Instance.isBuyer)
        {
            if (t_CarProfile.carCategory == CarCategory.MUSCLE)
            {
                quickSellButton.interactable = true;
            }
            else
            {
                quickSellButton.interactable = false;
            }
            
            quickSellButton.onClick.RemoveAllListeners();
            quickSellButton.onClick.AddListener(delegate
            {
                workshopCanvas.SetActive(false);
                UIManager.Instance.BuyerCarIns();
            });
        }
        else
        {
            quickSellButton.interactable = true;
            quickSellButton.onClick.RemoveAllListeners();
            quickSellButton.onClick.AddListener(delegate
            {
                //workshopCanvas.SetActive(false);
                //StartCoroutine(QuickSellRoutine());
                SellPanelInfo(SlidingController.Instance.GetCarProfile());
            });
        }
    }

    public void SellPanelInfo(CarProfile t_CarProfile)
    {
        sellingUIPanel.SetActive(true);
        carImage.sprite = AssetContainer.Instance.GetCarWithCategory(t_CarProfile.carCategory).carSprite;
        carTypeText.text = t_CarProfile.name;
        sellText.text = t_CarProfile.GetRestoredValue().ToString();
        boughtText.text = t_CarProfile.askingPrice.ToString();
        profitText.text = (t_CarProfile.GetRestoredValue() - t_CarProfile.askingPrice).ToString();
        
        sellCancelButton.onClick.AddListener(delegate
        {
            sellingUIPanel.SetActive(false);
        });
        sellPanelButton.onClick.AddListener(delegate
        {
            sellingUIPanel.SetActive(false);
            workshopCanvas.SetActive(false);
            StartCoroutine(QuickSellRoutine());
            
            UIManager.Instance.PlayDollar();
        });

    }

    IEnumerator QuickSellRoutine()
    {
        
        Debug.LogError("Quick SEELL ROUTONE ");
        yield return new WaitForEndOfFrame();

        BasicCoinTracker._coinMan.ChangeBy(CurrencyType.COIN, SlidingController.Instance.GetCarProfile().GetRestoredValue());
        
        
        GameManagerGM.Instance.RemoveCarFromSaved(SlidingController.Instance.GetCarProfile(),SlidingController.Instance.GetCarProfile().levelNumber);
        
        GameManagerGM.Instance.SaveData();
        GameManagerGM.Instance.LoadData();
            
        SlidingController.Instance.ResetCarHolder();
        
        yield return new WaitForEndOfFrame();
        isQuickSell = true;
        
        SlidingController.Instance.InstantiateCar(GameManagerGM.Instance.GetAllSavedCarasAList());
        
        yield return new WaitForEndOfFrame();
        workshopCanvas.SetActive(true);
        
        if (GameManagerGM.Instance.GetAllSavedCarasAList().Count == 0)
        {
            infoPanel.SetActive(false);
            leftPodiumButton.gameObject.SetActive(false);
            rightPodiumButton.gameObject.SetActive(false);
        }
        else
        {
            ShowInfoPanel(SlidingController.Instance.GetCarProfile());
        }
    }
    private void SetBodyBar(int t_BodyValue)
    {
        ResetBodyBar();
        for (int i = 0; i < t_BodyValue; i++)
        {
            bodyBarList[i].enabled = true;
        }

        if (bodyBarList.Count == t_BodyValue)
        {
            bodyRepairButton.gameObject.SetActive(false);
        }
        else
        {
            bodyRepairButton.gameObject.SetActive(true);
        }
    }
    private void SetPerformanceBar(int t_Performance)
    {
        ResetPerformanceBar();
        for (int i = 0; i < t_Performance; i++)
        {
            performanceBarList[i].enabled = true;
        }

        if (performanceBarList.Count == t_Performance)
        {
            performanceRepairButton.gameObject.SetActive(false);
        }
        else
        {
            performanceRepairButton.gameObject.SetActive(true);
        }
    }
    private void ResetPerformanceBar()
    {
        for (int i = 0; i < performanceBarList.Count; i++)
        {
            performanceBarList[i].enabled = false;
        }
    }
    private void ResetBodyBar()
    {
        for (int i = 0; i < bodyBarList.Count; i++)
        {
            bodyBarList[i].enabled = false;
        }
    }
    

    private void ResetBodyRepairCompletionBar()
    {
        for (int i = 0; i < bodyRepairCompletionBarList.Count; i++)
        {
            bodyRepairCompletionBarList[i].enabled = false;
        }
    }
    

    public void ActivateShop()
    {
        workshopCanvas.SetActive(true);
        ShowInfoPanel(SlidingController.Instance.GetCarProfile());
        m_IsBodyRepairStarted = false;
    }

    public void ShopBackReset()
    {
        isShopBack = false;
    }
}
