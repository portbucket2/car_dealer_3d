﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolsBase : MonoBehaviour
{
    public Animator hammingAnimator;
    public Animator moveAnimator;
    
    private int ENTRY = Animator.StringToHash("entry");
    private int HAMMING = Animator.StringToHash("hammer");
    private int MOVE_RIGHT = Animator.StringToHash("moveright");
    private int MOVE_LEFT = Animator.StringToHash("moveleft");
    private int EXIT = Animator.StringToHash("exit");


    public void PlayEntry()
    {
        moveAnimator.SetTrigger(ENTRY);
    }

    public void PlayMoveRigh()
    {
        moveAnimator.SetTrigger(MOVE_RIGHT);
    }

    public void PlayMoveLeft()
    {
        moveAnimator.SetTrigger(MOVE_LEFT);
    }

    public void PlayExit()
    {
        moveAnimator.SetTrigger(EXIT);
    }

    public void PlayHamming()
    {
        hammingAnimator.SetTrigger(HAMMING);
    }

    public void ResetTrigger()
    {
       moveAnimator.ResetTrigger(EXIT);
    }

    public void DisableAnimator()
    {
        moveAnimator.enabled = false;
    }
}
