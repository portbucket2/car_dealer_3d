﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class SlidingController : MonoBehaviour
{
   public GameObject podiumHolder;
   
   public int maxItemNumber;
   public int fixedItemNumber;

   public int currentSelectedIndex;

   public GameObject carHolderObject;
   public List<Transform> carHolderList;

   public Dictionary<int,GameObject> brokenCarObjectDic;
   public Dictionary<int,GameObject> cleanCarObjectDic;

   private Dictionary<int, CarProfile> carProfileDicitonary;
   
   
   public static SlidingController Instance;

   public int slidingCounter;

   public int totalCarInstantiate;

   public int listCounter;
   
   private void Awake()
   {
      if (Instance == null)
      {
         Instance = this;
      }
      currentSelectedIndex = 2;
      carProfileDicitonary = new Dictionary<int, CarProfile>();
      
      brokenCarObjectDic = new Dictionary<int, GameObject>();
      cleanCarObjectDic = new Dictionary<int, GameObject>();
   }

   public void ResetCarHolder()
   {
      for (int i = 0; i < carHolderList.Count; i++)
      {
         if (carHolderList[i].GetComponent<PodiumSlot>().isOccupied)
         {
            carHolderList[i].GetComponent<PodiumSlot>().ResetAll();
         }
      }

      Debug.LogError(carHolderList.Count +" Car holder list coundt " + listCounter);

      if (carHolderList.Count != 1)
      {
         for (int i = listCounter-1; i >= 1; i--)
         {
            Debug.Log("destroy ng "  +carHolderList[i].name);
            Destroy(carHolderList[i].gameObject);
            carHolderList.RemoveAt(i);
         }
      }

      

     

      foreach (var cleanCar in cleanCarObjectDic)
      {
         Destroy(cleanCar.Value);
      }

      foreach (var brokenCar in brokenCarObjectDic)
      {
         Destroy(brokenCar.Value);
      }
      cleanCarObjectDic.Clear();
      brokenCarObjectDic.Clear();
      carProfileDicitonary.Clear();
      totalCarInstantiate = 0;
      podiumHolder.transform.localPosition = Vector3.zero;
      slidingCounter = 0;
   }


   public void InstantitateHolder(List<CarProfile> carList)
   {
      
      for (int i = 1; i < carList.Count; i++)
      {
         GameObject holder = Instantiate(carHolderObject, podiumHolder.transform.localPosition , Quaternion.identity, podiumHolder.transform);
         holder.transform.localPosition = Vector3.zero;
         holder.transform.localPosition += new Vector3(0, 0, 0.8f * i);
         holder.transform.localRotation = Quaternion.identity;
         
         holder.GetComponent<PodiumSlot>().slotIndex = i;
         
         carHolderList.Add(holder.transform);
      }

      listCounter = carHolderList.Count;
      
      Debug.Log(carHolderList.Count + " Card holder list count");
   }

   public void InstantiateCar(List<CarProfile> carList)
   {
      Debug.LogError(carList.Count + " Total car cound");
      List<CarProfile> t_CarList = carList;

      for (int i = 0; i < t_CarList.Count; i++)
      {
         //Debug.LogError(t_CarList[i].levelNumber + " Before instante");
      }
      
      InstantitateHolder(t_CarList);
      
      if (!CDGameplay.Instance.isBuyer && !WorkshopUI.Instance.isQuickSell)
      {
         Debug.LogError("DEFAULT");
         GameObject firstCarBrokenPrefab = AssetContainer.Instance.GetCarWithCategory(t_CarList[t_CarList.Count-1].carCategory).brokenCarPrefab;
         GameObject firstCarCleanPrefab = AssetContainer.Instance.GetCarWithCategory(t_CarList[t_CarList.Count-1].carCategory).cleanCarPrefab;

         GameObject broken = Instantiate(firstCarBrokenPrefab, carHolderList[0].position, Quaternion.identity, carHolderList[0]);
         GameObject clean = Instantiate(firstCarCleanPrefab, carHolderList[0].position, Quaternion.identity, carHolderList[0]);

         carHolderList[0].GetComponent<PodiumSlot>().brokenCar = broken;
         carHolderList[0].GetComponent<PodiumSlot>().cleanCar = clean;

         clean.GetComponent<CarEntry>().enabled = false;
         clean.transform.localScale = Vector3.one;
         clean.transform.localRotation = Quaternion.identity;
         clean.SetActive(false);

         broken.GetComponent<CarEntry>().enabled = false;
         broken.transform.localScale = Vector3.one;
         broken.transform.localRotation = Quaternion.identity;

         carHolderList[0].GetComponent<PodiumSlot>().isOccupied = true;
         carHolderList[0].GetComponent<PodiumSlot>().occupiedCarProfile = t_CarList[t_CarList.Count - 1]; //CDGameplay.Instance.GetCurrentBoughtCar();
         carHolderList[0].GetComponent<PodiumSlot>().levelNumber = t_CarList[t_CarList.Count - 1].levelNumber; //GameManagerGM.Instance.levelNumber;

         carHolderList[0].GetComponent<PodiumSlot>().occupiedCarProfile.levelNumber = t_CarList[t_CarList.Count - 1].levelNumber;//GameManagerGM.Instance.levelNumber;
      }

      //if (carList.Count != 1)
      {
         for (int i = 0; i < t_CarList.Count; i++)
         {
            Debug.Log(t_CarList[i].levelNumber);
            if (!carHolderList[i].GetComponent<PodiumSlot>().isOccupied)
            {
               Debug.LogError("IF");
               GameObject car0 = AssetContainer.Instance.GetCarWithCategory(t_CarList[i].carCategory).brokenCarPrefab;
               GameObject cleanCarPrefab =
                  AssetContainer.Instance.GetCarWithCategory(t_CarList[i].carCategory).cleanCarPrefab;

               GameObject t_Car0 = Instantiate(car0, carHolderList[i].position, Quaternion.identity, carHolderList[i]);
               GameObject t_CleanCar = Instantiate(cleanCarPrefab, carHolderList[i].position, Quaternion.identity,
                  carHolderList[i]);

               carHolderList[i].GetComponent<PodiumSlot>().brokenCar = t_Car0;
               carHolderList[i].GetComponent<PodiumSlot>().cleanCar = t_CleanCar;

               t_CleanCar.GetComponent<CarEntry>().enabled = false;
               t_CleanCar.transform.localScale = Vector3.one;
               t_CleanCar.transform.localRotation = Quaternion.identity;
               t_CleanCar.SetActive(false);

               t_Car0.GetComponent<CarEntry>().enabled = false;
               t_Car0.transform.localScale = Vector3.one;
               t_Car0.transform.localRotation = Quaternion.identity;


               carHolderList[i].GetComponent<PodiumSlot>().isOccupied = true;
               carHolderList[i].GetComponent<PodiumSlot>().occupiedCarProfile = t_CarList[i];
               carHolderList[i].GetComponent<PodiumSlot>().levelNumber = t_CarList[i].levelNumber;

               carProfileDicitonary.Add(carHolderList[i].GetComponent<PodiumSlot>().slotIndex, t_CarList[i]);

               brokenCarObjectDic.Add(carHolderList[i].GetComponent<PodiumSlot>().slotIndex, t_Car0);
               cleanCarObjectDic.Add(carHolderList[i].GetComponent<PodiumSlot>().slotIndex, t_CleanCar);

               carHolderList[i].GetComponent<PodiumSlot>().ActivateCleanCar();

            }
            else
            {
               if (t_CarList[i].levelNumber != CDGameplay.Instance.GetCurrentBoughtCar().levelNumber)
               {
                  Debug.LogError("ELSE");
                  GameObject car0 = AssetContainer.Instance.GetCarWithCategory(t_CarList[i].carCategory)
                     .brokenCarPrefab;
                  GameObject cleanCarPrefab = AssetContainer.Instance.GetCarWithCategory(t_CarList[i].carCategory)
                     .cleanCarPrefab;

                  GameObject t_Car0 = Instantiate(car0, carHolderList[i + 1].position, Quaternion.identity,
                     carHolderList[i + 1]);
                  GameObject t_CleanCar = Instantiate(cleanCarPrefab, carHolderList[i + 1].position,
                     Quaternion.identity, carHolderList[i + 1]);

                  carHolderList[i + 1].GetComponent<PodiumSlot>().brokenCar = t_Car0;
                  carHolderList[i + 1].GetComponent<PodiumSlot>().cleanCar = t_CleanCar;

                  t_CleanCar.GetComponent<CarEntry>().enabled = false;
                  t_CleanCar.transform.localScale = Vector3.one;
                  t_CleanCar.transform.localRotation = Quaternion.identity;
                  t_CleanCar.SetActive(false);

                  t_Car0.GetComponent<CarEntry>().enabled = false;
                  t_Car0.transform.localScale = Vector3.one;
                  t_Car0.transform.localRotation = Quaternion.identity;


                  carHolderList[i + 1].GetComponent<PodiumSlot>().isOccupied = true;

                  carHolderList[i + 1].GetComponent<PodiumSlot>().occupiedCarProfile = t_CarList[i];
                  carHolderList[i + 1].GetComponent<PodiumSlot>().levelNumber = t_CarList[i].levelNumber;

                  if (!carProfileDicitonary.ContainsKey(carHolderList[i + 1].GetComponent<PodiumSlot>().slotIndex))
                  {
                     carProfileDicitonary.Add(carHolderList[i + 1].GetComponent<PodiumSlot>().slotIndex, t_CarList[i]);
                  }

                  brokenCarObjectDic.Add(carHolderList[i + 1].GetComponent<PodiumSlot>().slotIndex, t_Car0);
                  cleanCarObjectDic.Add(carHolderList[i + 1].GetComponent<PodiumSlot>().slotIndex, t_CleanCar);

                  carHolderList[i + 1].GetComponent<PodiumSlot>().ActivateCleanCar();
               }
            }
         }
      }

      for (int i = 0; i < carHolderList.Count; i++)
      {
         if (carHolderList[i].GetComponent<PodiumSlot>().isOccupied)
         {
            totalCarInstantiate++;
           // Debug.LogError(totalCarInstantiate + " Total car instantiate");
         }
      }
   }

   public void ActivateCleanCar()
   {
      //if (brokenCarObjectDic.ContainsKey(currentSelectedIndex))
     // {
      //   brokenCarObjectDic[currentSelectedIndex].SetActive(false);
     // }

     // if (cleanCarObjectDic.ContainsKey(currentSelectedIndex))
     // {
     //    cleanCarObjectDic[currentSelectedIndex].SetActive(true);
     // }


      carHolderList[Mathf.Abs(slidingCounter)].GetComponent<PodiumSlot>().brokenCar.SetActive(false);
      carHolderList[Mathf.Abs(slidingCounter)].GetComponent<PodiumSlot>().cleanCar.SetActive(true);
   }
   


   public void RightMovement()
   {
      currentSelectedIndex++;
      if (currentSelectedIndex > 0)
      {
         currentSelectedIndex = 0;
      }

      slidingCounter--;
      if (slidingCounter < -totalCarInstantiate+1)
      {
         slidingCounter = -totalCarInstantiate+1;
      }

      Vector3 t_Dest =  new Vector3(0, 0, 0.8f * slidingCounter);
      StartCoroutine(ZAxisMoveRoutine(t_Dest, podiumHolder, delegate { }));
   }

   public void LeftMovement()
   {
      currentSelectedIndex--;
      if (currentSelectedIndex < 0)
      {
         currentSelectedIndex = 0;
      }
      slidingCounter++;

      if (slidingCounter > 0)
      {
         slidingCounter = 0;
      }

      Vector3 t_Dest =  new Vector3(0, 0, 0.8f*slidingCounter);
      StartCoroutine(ZAxisMoveRoutine(t_Dest, podiumHolder, delegate { }));
   }


   public IEnumerator ZAxisMoveRoutine(Vector3 t_Dest,GameObject t_TargetTrans,UnityAction t_Action = null)
   {
      float t_Progression = 0f;
      float t_Duration = .5f;
      float t_EndTIme = Time.time + t_Duration;
      Vector3 t_CurrentFillValue = t_TargetTrans.transform.localPosition;
      Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);
             
     
      WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
     
      //Platform Movement
      while (true)
      {
         t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
    
         t_TargetTrans.transform.localPosition= new Vector3(Mathf.Lerp(t_CurrentFillValue.x,t_DestValue.x,t_Progression),
            t_TargetTrans.transform.localPosition.y,Mathf.Lerp(t_CurrentFillValue.z,t_DestValue.z,t_Progression));
     
         if (t_Progression >= 1f)
         {
            if (t_Action != null)
            {
               t_Action.Invoke();
            }

            break;
         }
     
         yield return t_CycleDelay;
      }
          
      StopCoroutine(ZAxisMoveRoutine(Vector3.zero,null,null));
   }


   public CarProfile GetCarProfile()
   {
      CarProfile profile = new CarProfile();
     // if (carProfileDicitonary.ContainsKey(currentSelectedIndex))
      //{
      //   profile = carProfileDicitonary[currentSelectedIndex];
     // }

     profile = carHolderList[Mathf.Abs(slidingCounter)].GetComponent<PodiumSlot>().occupiedCarProfile;
      
      
      return profile;
   }

   public int GetSelectedCarLevelNumber()
   {
      return carHolderList[Mathf.Abs(slidingCounter)].GetComponent<PodiumSlot>().levelNumber;
   }
}
