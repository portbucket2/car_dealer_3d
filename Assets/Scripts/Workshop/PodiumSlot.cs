﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PodiumSlot : MonoBehaviour
{
    public bool isOccupied;
    public int slotIndex;
    public GameObject brokenCar;
    public GameObject cleanCar;
    public int levelNumber;
    
    public CarProfile occupiedCarProfile;

    private void Awake()
    {
        occupiedCarProfile = null;
        occupiedCarProfile = new CarProfile();
    }

    public void ActivateCleanCar()
    {
        if (occupiedCarProfile.isPolished)
        {
            brokenCar.SetActive(false);
            cleanCar.SetActive(true);
        }
    }

    public void ResetAll()
    {
        isOccupied = false;
        
        brokenCar = null;
        cleanCar = null;
        Destroy(transform.GetChild(0).gameObject);
        Destroy(transform.GetChild(1).gameObject);
    }
}
