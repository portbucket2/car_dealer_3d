﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkshopController : MonoBehaviour
{
    public ToolsBase hammerTool;
    public ToolsBase drillTool;
    public ToolsBase scerwTool;

    
    public ParticleSystem smokeParticleBig;

    public static WorkshopController Instance;

    public WorkshopUI workshopUI;
    
    
    
    

    
    

    private Coroutine m_HammerRoutine;
    private Coroutine m_DrillRoutine;
    private Coroutine m_ScrewRoutine;
    private Coroutine m_ToolsRoutine;
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        ////m_HammerRoutine = HammerRoutine();
        //m_DrillRoutine = DrillRoutine();
       // m_ScrewRoutine = ScrewRoutine();
       
    }

    public void StartBodyRepair()
    {
       m_ToolsRoutine = StartCoroutine(ToolsRoutine());
    }

    public void StopBodyRepair()
    {
        StartCoroutine(StopRepairRoutine());
    }

    IEnumerator StopRepairRoutine()
    {
        StopCoroutine(m_ToolsRoutine);
        StopCoroutine(m_HammerRoutine);
        StopCoroutine(m_DrillRoutine);
        StopCoroutine(m_ScrewRoutine);
        
        hammerTool.PlayExit();
        drillTool.PlayExit();
        scerwTool.PlayExit();
        smokeParticleBig.Stop();
        
        yield return new WaitForSeconds(0.5f);
        hammerTool.moveAnimator.enabled = false;
        drillTool.moveAnimator.enabled = false;
        scerwTool.moveAnimator.enabled = false;
    }

    IEnumerator ToolsRoutine()
    {
       m_HammerRoutine = StartCoroutine(HammerRoutine());
       m_DrillRoutine = StartCoroutine(DrillRoutine());
       m_ScrewRoutine = StartCoroutine(ScrewRoutine());
        yield return new WaitForSeconds(2);
        yield return new WaitForSeconds(5);
        smokeParticleBig.Play();
        yield return new WaitForSeconds(2);
        //bokenCar.SetActive(false);
        // cleanCar.SetActive(true);
        SlidingController.Instance.ActivateCleanCar();
    }

    IEnumerator HammerRoutine()
    {
        hammerTool.moveAnimator.enabled = true;
        
        hammerTool.ResetTrigger();
        yield return new WaitForEndOfFrame();
        hammerTool.PlayEntry();
        yield return new WaitForSeconds(1f);
        hammerTool.PlayHamming();
        
        yield return new WaitForSeconds(1f);
        hammerTool.PlayMoveRigh();
        
        yield return new WaitForSeconds(2.5f);
        hammerTool.PlayMoveLeft();
        
        
        yield return new WaitForSeconds(2.5f);
        hammerTool.PlayMoveRigh();
        
        yield return new WaitForSeconds(2.5f);
        hammerTool.PlayMoveLeft();
        
        yield return new WaitForSeconds(2.5f);
        hammerTool.PlayExit();
        
        yield return new WaitForSeconds(1.5f);
        hammerTool.DisableAnimator();
    }
    
    IEnumerator DrillRoutine()
    {
        drillTool.moveAnimator.enabled = true;
        drillTool.ResetTrigger();
        yield return new WaitForEndOfFrame();
        drillTool.PlayEntry();
        yield return new WaitForSeconds(1f);
        drillTool.PlayHamming();
        
        yield return new WaitForSeconds(1f);
        drillTool.PlayMoveRigh();
        
        yield return new WaitForSeconds(2.5f);
        drillTool.PlayMoveLeft();
        
        
        yield return new WaitForSeconds(2.5f);
        drillTool.PlayMoveRigh();
        
        yield return new WaitForSeconds(2.5f);
        drillTool.PlayMoveLeft();
        
        yield return new WaitForSeconds(2.5f);
        drillTool.PlayExit();
        
        yield return new WaitForSeconds(1.5f);
        drillTool.DisableAnimator();
    }
    IEnumerator ScrewRoutine()
    {
        scerwTool.moveAnimator.enabled = true;
        scerwTool.ResetTrigger();
        
        yield return new WaitForEndOfFrame();
        scerwTool.PlayEntry();
        yield return new WaitForSeconds(1f);
        scerwTool.PlayHamming();
        
        yield return new WaitForSeconds(1f);
        scerwTool.PlayMoveRigh();
        
        yield return new WaitForSeconds(3f);
        scerwTool.PlayMoveLeft();
        
        
        yield return new WaitForSeconds(3f);
        scerwTool.PlayMoveRigh();
        
        yield return new WaitForSeconds(3f);
        scerwTool.PlayMoveLeft();
        
        yield return new WaitForSeconds(2f);
        scerwTool.PlayExit();
        
        yield return new WaitForSeconds(1.5f);
        scerwTool.DisableAnimator();
    }
    
}
