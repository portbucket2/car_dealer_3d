﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gameplay : MonoBehaviour
{
    public static Gameplay Instance;
    public int currentLevel;
    public GameObject miniOnePrefab;

    

    private void Awake()
   {
      if (Instance == null)
      {
         Instance = this;
      }

        

   }

    private void Start()
    {
        //currentLevel = LevelManager.Instance.GetCurrentLevel();
    }

    public void StartGame()
   {
       
        StartCoroutine(StartGameRoutine());
   }

   private IEnumerator StartGameRoutine()
   {
        if(currentLevel > 3)
        {
            currentLevel = 1;
        }
        yield return new WaitForEndOfFrame();
        UIManager.Instance.sold.SetActive(false);
       

        CarEntry.m_Instance.EntrySequence();
        CharController.instance.EnteyCharacter();
        yield return new WaitUntil(() => CharController.instance.charExited == true);
        UIManager.Instance.DisplayInfo();
        
        yield return new WaitUntil(() => CharController.instance.charEntered == true);
        //UIManager.Instance.SoldPanel();
        GameObject miniOne = Instantiate(miniOnePrefab, new Vector3(0f,2.2f,-40f), Quaternion.identity);

        yield return new WaitUntil(() => CarEntry.instance.carRejected == true);
        UIManager.Instance.RejectPanel();



    }
}
