﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PoliceController : MonoBehaviour
{
    public static PoliceController instance;

    [SerializeField]
    private Vector3[] charPos;
    [SerializeField]
    private Vector3[] charRot;

    [SerializeField]
    private float moveDuration;
    [SerializeField]
    private float rotDuration;

    [SerializeField]
    public Animator charAnim;

    public bool isArrest;

    private int WALK = Animator.StringToHash("walk");
    private int WALK_POLICE = Animator.StringToHash("police");
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }

    }

    private void Start()
    {
        charAnim = GetComponent<Animator>();
    }

    public void TriggerAnimation(int trigger)
    {
        charAnim.SetTrigger(trigger);
        charAnim.speed = 2f;
    }
    public void PoliceBehavior()
    {
        StartCoroutine(PoliceSequence());
    }

    IEnumerator PoliceSequence()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(.8f);
        TriggerAnimation(WALK_POLICE);
        transform.DOMove(charPos[0], moveDuration).OnComplete(()=> isArrest = true);
        yield return new WaitForSeconds(3f);
        //isArrest = false;
        transform.position = new Vector3(10, -0.577f, -4);

    }

    public void ResetArrest()
    {
        isArrest = false;
    }
}
