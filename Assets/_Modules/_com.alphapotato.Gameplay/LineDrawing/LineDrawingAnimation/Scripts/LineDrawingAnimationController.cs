﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LineDrawingAnimationController : MonoBehaviour
{
    #region Public Variables

    public LineRenderer lineRendererReference;
    public Vector3 defaultLineDrawingOffset;
    [Range(0f,25f)]
    public float defaultAnimationSpeed;
    [Range(0f,5f)]
    public float defaultCompletationDelay;
    [Range(0f,3f)]
    public float defaultDurationForFadingOut;

    #endregion

    #region Private Variables

    //[SerializeField]
    private bool m_IsLineDrawingAnimationControllerRunning;

    //[SerializeField]
    private int m_CurrentIndexForLineDrawingTarget;
    //[SerializeField]
    private int m_NumberOfPivotPoints;

    //[SerializeField]
    private float m_AnimationSpeed;
    private float m_CompletationDelay;
    private float m_DurationForFadingOut;

    private Gradient m_InitialColorGradient;

    private UnityAction OnAnimationEnd;
    private UnityAction OnDelayEnd;
    private UnityAction OnFadingOutEnd;

    //[SerializeField]
    private List<Vector3> m_PivotPoints;
    private Vector3 m_LineDrawingOffset;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        m_InitialColorGradient = lineRendererReference.colorGradient;
    }

    #endregion

    #region Configuretion

    private IEnumerator ControllerForLineDrawingAnimation() {

        float t_CycleLength = 0.0167f;
        WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

        Vector3 t_ModifiedPosition;

        while (m_IsLineDrawingAnimationControllerRunning) {

            if (Vector3.Distance(m_PivotPoints[m_CurrentIndexForLineDrawingTarget] + m_LineDrawingOffset, lineRendererReference.GetPosition(m_CurrentIndexForLineDrawingTarget)) <= 0.1f) {

                m_CurrentIndexForLineDrawingTarget++;
                lineRendererReference.positionCount = m_CurrentIndexForLineDrawingTarget + 1;
                lineRendererReference.SetPosition(m_CurrentIndexForLineDrawingTarget, m_PivotPoints[m_CurrentIndexForLineDrawingTarget - 1] + m_LineDrawingOffset);

            }

            if (m_CurrentIndexForLineDrawingTarget >= m_NumberOfPivotPoints)
            {
                break;
            }

            t_ModifiedPosition = Vector3.MoveTowards(
                    lineRendererReference.GetPosition(m_CurrentIndexForLineDrawingTarget),
                    m_PivotPoints[m_CurrentIndexForLineDrawingTarget] + m_LineDrawingOffset,
                    m_AnimationSpeed * Time.deltaTime
                );
            lineRendererReference.SetPosition(m_CurrentIndexForLineDrawingTarget, t_ModifiedPosition);

            yield return t_CycleDelay;
        }


        OnAnimationEnd?.Invoke();
        yield return new WaitForSeconds(m_CompletationDelay);
        OnDelayEnd?.Invoke();

        yield return  StartCoroutine(ControllerForFadingOut());
        OnFadingOutEnd?.Invoke();

        m_IsLineDrawingAnimationControllerRunning = false;

        StopCoroutine(ControllerForLineDrawingAnimation());

    }

    private IEnumerator ControllerForFadingOut() {

        float t_CycleLength = 0.0167f;
        WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

        float t_RemainingTimeForFading = m_DurationForFadingOut;

        Gradient t_ColorGradientOfLineRenderer  = lineRendererReference.colorGradient;
        GradientAlphaKey[] t_AlphaKey           = lineRendererReference.colorGradient.alphaKeys; 
        
        int t_NumberOfAlphaKey = t_ColorGradientOfLineRenderer.alphaKeys.Length - 1;
        for (int i = 0; i <= t_NumberOfAlphaKey; i++)
        {
            t_AlphaKey[i].alpha = (t_NumberOfAlphaKey - i) / ((float)t_NumberOfAlphaKey);
        }
        t_ColorGradientOfLineRenderer.SetKeys(t_ColorGradientOfLineRenderer.colorKeys, t_AlphaKey);

        Color t_ProgressedColor;

        while (t_RemainingTimeForFading > 0) {

            t_ProgressedColor                   = t_ColorGradientOfLineRenderer.Evaluate(1f - (t_RemainingTimeForFading / m_DurationForFadingOut));
            lineRendererReference.startColor    = t_ProgressedColor;
            lineRendererReference.endColor      = t_ProgressedColor;

            yield return t_CycleDelay;
            t_RemainingTimeForFading -= t_CycleLength;
        }

        t_ProgressedColor = t_ColorGradientOfLineRenderer.Evaluate(1f);
        lineRendererReference.startColor = t_ProgressedColor;
        lineRendererReference.endColor = t_ProgressedColor;
        m_AnimationSpeed = 0f;

        StopCoroutine(ControllerForFadingOut());
    }

    #endregion

    #region Public Callback

    public void ShowLineDrawingAnimation(
        List<Vector3> t_PivotPoints,
        Vector3 t_LineDrawingOffset = new Vector3(),
        float t_AnimationSpeed = 0,
        float t_CompletationDelay = 0,
        float t_DurationForFadingOut = 0,
        UnityAction OnAnimationEnd = null,
        UnityAction OnDelayEnd = null,
        UnityAction OnFadingOutEnd = null)
    {

        m_LineDrawingOffset = t_LineDrawingOffset;

        if (m_AnimationSpeed == 0)
            m_AnimationSpeed = defaultAnimationSpeed;
        else
            m_AnimationSpeed = t_AnimationSpeed;

        if (t_CompletationDelay == 0)
            m_CompletationDelay = defaultCompletationDelay;
        else
            m_CompletationDelay = t_CompletationDelay;

        if (t_DurationForFadingOut == 0)
            m_DurationForFadingOut = defaultDurationForFadingOut;
        else
            m_DurationForFadingOut = t_DurationForFadingOut;

        this.OnAnimationEnd = OnAnimationEnd;
        this.OnDelayEnd     = OnDelayEnd;
        this.OnFadingOutEnd = OnFadingOutEnd;

        m_PivotPoints                       = t_PivotPoints;
        m_CurrentIndexForLineDrawingTarget  = 1;
        m_NumberOfPivotPoints               = m_PivotPoints.Count;
        lineRendererReference.positionCount = 2;
        lineRendererReference.SetPosition(0, m_PivotPoints[0] + m_LineDrawingOffset);
        lineRendererReference.SetPosition(1, m_PivotPoints[0] + m_LineDrawingOffset);

        lineRendererReference.startColor = m_InitialColorGradient.Evaluate(0);
        lineRendererReference.endColor = m_InitialColorGradient.Evaluate(1);

        if (!m_IsLineDrawingAnimationControllerRunning)
        {
            m_IsLineDrawingAnimationControllerRunning = true;
            StartCoroutine(ControllerForLineDrawingAnimation());
        }
    }

    #endregion
}
