﻿namespace com.alphapotato.Gameplay
{
    using UnityEngine;
    using UnityEngine.Events;
    using com.faithstudio.Gameplay;

    public class PowerBarController : MonoBehaviour
    {
        #region Custom Variables

        public delegate void DelegateForPassingTheValueOfAccuracy(float t_Value);
        public DelegateForPassingTheValueOfAccuracy OnPassingTheAccuracyEvaluation;

        public enum TypeOfPowerBar
        {
            PerfectOnMin,
            PerfectOnMax,
            PerfectOnCenter
        }

        #endregion

        #region  Public Variables

        [Header("Reference      :   External")]
        public GameObject powerBarFBX;
        public Transform powerBarIndicator;
        public Animator animatorReferenceForPowerBar;

        [Header("Configuretion  :   PowerBar")]
        public TypeOfPowerBar typeOfPowerBar = TypeOfPowerBar.PerfectOnCenter;
        [ConditionalHide("typeOfPowerBar", 0)]
        public AnimationCurve curveForAngulerVelocityWhenPerfectOnMin = new AnimationCurve(new Keyframe[] { new Keyframe(0f, 1f), new Keyframe(1f, 0.1f) });
        [ConditionalHide("typeOfPowerBar", 1)]
        public AnimationCurve curveForAngulerVelocityWhenPerfectOnMax = new AnimationCurve(new Keyframe[] { new Keyframe(0f, 0.1f), new Keyframe(1f, 1f) });
        [ConditionalHide("typeOfPowerBar", 2)]
        public AnimationCurve curveForAngulerVelocityWhenPerfectOnCenter = new AnimationCurve(new Keyframe[] { new Keyframe(0f, 0.1f), new Keyframe(0.5f, 1f), new Keyframe(1f, 0.1f) });

        [Range(0f, 360)]
        public float rotationOnMin = 90;
        [Range(0f, 360)]
        public float rotationOnMax = 270;
        [Range(0.05f, 0.25f)]
        public float boundaryArea = 0.1f;

        [Space(5.0f)]
        [Range(0f, 500f)]
        public float defaultAngulerVelocity = 1f;

        #endregion

        #region  Private Variables

        private TimeController m_TimeControllerReference;

        private bool m_IsPowerMeterShowing = false;
        private bool m_IsGoingToTargetedRotation;

        private float m_BoundaryArea;
        private float m_DifferenceForRotation;
        private float m_AngulerVelocity;
        private float m_InterpolatedPositionOfIndicator;

        private float m_InitialRotation;
        private float m_EndRotation;
        private float m_CurrentRotation;
        private float m_TargetedRotation;

        private AnimationCurve m_ActiveCurveForAngulerVelocity;

        private UnityAction OnPowerBarSet;

        #endregion

        #region  Mono Behaviour

        private void Awake()
        {

            m_DifferenceForRotation = Mathf.Abs(rotationOnMax - rotationOnMin);
            m_BoundaryArea = Mathf.Abs(rotationOnMax - rotationOnMin) * boundaryArea;
            enabled = false;
        }

        private void Update()
        {

            ControllerForRotationOfIndicator();
        }

        #endregion

        #region Configuretion

        private void OnTouchDown(Vector3 t_TouchPosition){
            
            OnPassingTheAccuracyEvaluation(ReturnThePerformance());
            OnPowerBarSet.Invoke();
            HidePowerBar();
        }

        private void ControllerForRotationOfIndicator()
        {

            m_CurrentRotation = Mathf.MoveTowards(m_CurrentRotation, m_TargetedRotation, m_TimeControllerReference.GetAbsoluteDeltaTime() * m_AngulerVelocity * m_ActiveCurveForAngulerVelocity.Evaluate(m_InterpolatedPositionOfIndicator));
            m_InterpolatedPositionOfIndicator = Mathf.Abs((m_EndRotation - m_CurrentRotation) / (m_EndRotation - m_InitialRotation));
            Vector3 t_CurrentRotation = powerBarIndicator.localEulerAngles;
            Vector3 t_ModifiedEulerAngle = new Vector3(
                t_CurrentRotation.x,
                t_CurrentRotation.y,
                m_CurrentRotation
            );
            powerBarIndicator.localEulerAngles = t_ModifiedEulerAngle;

            if (Mathf.Abs(m_CurrentRotation - m_TargetedRotation) <= m_BoundaryArea)
            {

                m_IsGoingToTargetedRotation = !m_IsGoingToTargetedRotation;
                if (m_IsGoingToTargetedRotation)
                    m_TargetedRotation = m_EndRotation;
                else
                    m_TargetedRotation = m_InitialRotation;

            }
        }

        #endregion

        #region Public Callback

        public void ShowPowerBar(UnityAction OnPowerBarSet = null, float t_VelocityForPowerBar = 0)
        {
            if (!m_IsPowerMeterShowing)
            {

                GlobalTouchController.Instance.OnTouchDown += OnTouchDown;

                this.OnPowerBarSet = OnPowerBarSet;

                if (t_VelocityForPowerBar == 0)
                    m_AngulerVelocity = defaultAngulerVelocity;
                else
                    m_AngulerVelocity = t_VelocityForPowerBar;

                m_InterpolatedPositionOfIndicator = 0;
                m_TimeControllerReference = TimeController.Instance;

                switch (typeOfPowerBar)
                {

                    case TypeOfPowerBar.PerfectOnMin:

                        powerBarIndicator.localEulerAngles = powerBarIndicator.localEulerAngles + (Vector3.forward * (rotationOnMax - m_BoundaryArea));
                        m_InitialRotation = rotationOnMax;
                        m_EndRotation = rotationOnMin;

                        m_ActiveCurveForAngulerVelocity = curveForAngulerVelocityWhenPerfectOnMin;

                        break;
                    case TypeOfPowerBar.PerfectOnMax:

                        powerBarIndicator.localEulerAngles = powerBarIndicator.localEulerAngles + (Vector3.forward * (rotationOnMax + m_BoundaryArea));

                        m_InitialRotation = rotationOnMin;
                        m_EndRotation = rotationOnMax;

                        m_ActiveCurveForAngulerVelocity = curveForAngulerVelocityWhenPerfectOnMax;

                        break;
                    case TypeOfPowerBar.PerfectOnCenter:

                        if (Random.Range(0f, 1f) <= 0.5f)
                        {

                            powerBarIndicator.localEulerAngles = powerBarIndicator.localEulerAngles + (Vector3.forward * (rotationOnMax - m_BoundaryArea));
                            m_InitialRotation = rotationOnMax;
                            m_EndRotation = rotationOnMin;
                        }
                        else
                        {

                            powerBarIndicator.localEulerAngles = powerBarIndicator.localEulerAngles + (Vector3.forward * (rotationOnMax + m_BoundaryArea));
                            m_InitialRotation = rotationOnMin;
                            m_EndRotation = rotationOnMax;
                        }

                        m_ActiveCurveForAngulerVelocity = curveForAngulerVelocityWhenPerfectOnCenter;

                        break;
                }

                m_CurrentRotation = m_InitialRotation;
                m_TargetedRotation = m_EndRotation;

                if (animatorReferenceForPowerBar != null)
                {
                    animatorReferenceForPowerBar.SetTrigger("APPEAR");
                }
                else
                {
                    if (!powerBarFBX.activeInHierarchy)
                        powerBarFBX.SetActive(true);
                }

                m_IsGoingToTargetedRotation = true;
                m_IsPowerMeterShowing = true;
                enabled = true;
            }
        }

        public void HidePowerBar()
        {
            if (m_IsPowerMeterShowing)
            {
                enabled = false;

                GlobalTouchController.Instance.OnTouchDown -= OnTouchDown;

                if (animatorReferenceForPowerBar != null)
                {

                    animatorReferenceForPowerBar.SetTrigger("DISAPPEAR");
                }
                else
                {

                    if (powerBarFBX.activeInHierarchy)
                        powerBarFBX.SetActive(false);
                }

                m_IsPowerMeterShowing = false;
            }
        }

        public float ReturnThePerformance()
        {
            float t_ValueOfPerformance = 0;
            if (typeOfPowerBar == TypeOfPowerBar.PerfectOnCenter)
            {
                t_ValueOfPerformance =  1f - (Mathf.Abs(0.5f - m_InterpolatedPositionOfIndicator) / 0.5f);
              
            }else{

                t_ValueOfPerformance = 1f - m_InterpolatedPositionOfIndicator;
            }
            return t_ValueOfPerformance;
        }


        #endregion
    }
}


