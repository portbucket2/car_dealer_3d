﻿using System;
using System.Collections;
using UnityEngine;



public class GearMinigame : MonoBehaviour, IMinigame
{
    public GameObject correctform;
    public Camera mainCam;
    public bool moving;
    public bool finish;
    public Animator animator;

    private float startPosX;
    private float startPosY;

    private Vector3 resetPosition;

    public bool isReset;
    
    //public event Action OnGearMinigameEnd;

    void Awake()
    {
        resetPosition = transform.localPosition;
        animator.enabled = false;
    }

    void Update()
    {
        if (finish == false)
        {
            if (moving)
            {
                Vector3 mousePos;
                mousePos = Input.mousePosition;
                mousePos = mainCam.ScreenToWorldPoint(mousePos);

                gameObject.transform.position = new Vector3(mousePos.x - startPosX, mousePos.y - startPosY, this.gameObject.transform.position.z);
            }
        }

        if (isReset)
        {
            Reset();
            isReset = false;
        }
    }
    private void OnMouseDown()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos;
            mousePos = Input.mousePosition;
            mousePos = mainCam.ScreenToWorldPoint(mousePos);

            startPosX = mousePos.x - this.transform.position.x;
            startPosY = mousePos.y - this.transform.position.y;

            moving = true;
        }
    }
    private void OnMouseUp()
    {
        moving = false;

        if (Mathf.Abs(this.transform.position.x - correctform.transform.position.x) <= 0.5f &&
            Mathf.Abs(this.transform.position.y - correctform.transform.position.y) <= 0.5f)
        {
            transform.position = new Vector3(correctform.transform.position.x, correctform.transform.position.y, correctform.transform.position.z);
            transform.SetParent(correctform.transform);
            animator.enabled = true;
            finish = true;
            StartCoroutine(WaitAndEnd());
        }
        else
        {
            transform.localPosition = new Vector3(resetPosition.x, resetPosition.y, resetPosition.z);
        }
    }

    IEnumerator WaitAndEnd()
    {
        yield return new WaitForSeconds(2f);
        OnMinigameEnd?.Invoke();
        Debug.Log("Gear Minigame Ended!");
    }

    public void ActivateMinigame()
    {
    }

    public void DeactivateMinigame()
    {
    }

    public void Reset()
    {
        transform.localPosition = resetPosition;
        animator.enabled = false;
        finish = false;
    }

    public event Action OnMinigameEnd;
}