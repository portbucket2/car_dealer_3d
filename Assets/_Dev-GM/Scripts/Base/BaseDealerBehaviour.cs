﻿using UnityEngine;

public class BaseCarDealerBehaviour : MonoBehaviour
{
    [HideInInspector]
    public GameState gameState;

    public virtual void Awake()
    {

    }

    public virtual void OnEnable()
    {
        BaseCarDealerManager.OnNone += OnNone;
        BaseCarDealerManager.OnIdle += OnIdle;
        BaseCarDealerManager.OnLevelInitializing += OnLevelInitializing;
        BaseCarDealerManager.OnLevelStarting += OnLevelStarting;
        BaseCarDealerManager.OnCustomerArriving += OnCustomerArriving;
        BaseCarDealerManager.OnCustomerItemScanning += OnCustomerItemScanning;
        BaseCarDealerManager.OnCustomerItemDealingStart += OnCustomerItemDealingStart;
        BaseCarDealerManager.OnCustomerItemDealingEnd += OnCustomerItemDealingEnd;
        BaseCarDealerManager.OnCustomerLeavingShop += OnCustomerLeavingShop;
        BaseCarDealerManager.OnLevelEnding += OnLevelEnding;
        BaseCarDealerManager.OnChangeGameState += OnChangeGameState;
    }

    public virtual void OnDisable()
    {
        BaseCarDealerManager.OnNone -= OnNone;
        BaseCarDealerManager.OnIdle -= OnIdle;
        BaseCarDealerManager.OnLevelInitializing -= OnLevelInitializing;
        BaseCarDealerManager.OnLevelStarting -= OnLevelStarting;
        BaseCarDealerManager.OnCustomerArriving -= OnCustomerArriving;
        BaseCarDealerManager.OnCustomerItemScanning -= OnCustomerItemScanning;
        BaseCarDealerManager.OnCustomerItemDealingStart -= OnCustomerItemDealingStart;
        BaseCarDealerManager.OnCustomerItemDealingEnd -= OnCustomerItemDealingEnd;
        BaseCarDealerManager.OnCustomerLeavingShop -= OnCustomerLeavingShop;
        BaseCarDealerManager.OnLevelEnding -= OnLevelEnding;
        BaseCarDealerManager.OnChangeGameState -= OnChangeGameState;
    }

    public virtual void OnChangeGameState(GameState gameState)
    {
        this.gameState = gameState;
    }

    public virtual void OnNone()
    {
    }

    public virtual void OnIdle(GameplayData gameplayData)
    {
    }

    public virtual void OnLevelInitializing(LevelInitializingData levelInitializingData)
    {
    }

    public virtual void OnLevelStarting()
    {
    }

    public virtual void OnCustomerArriving()
    {
    }

    public virtual void OnCustomerItemScanning()
    {
    }

    public virtual void OnCustomerItemDealingStart()
    {
    }

    public virtual void OnCustomerItemDealingEnd(CustomerInputType customerInputType)
    {
    }

    public virtual void OnCustomerLeavingShop()
    {
    }

    public virtual void OnLevelEnding(GameplayData gameplayData)
    {
    }


}
