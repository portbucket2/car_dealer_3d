﻿using UnityEngine;
using System;

public class BaseCarDealerManager : MonoBehaviour
{
    public static BaseCarDealerManager instance;

    public static event Action OnNone;
    public static event Action<GameplayData> OnIdle;
    public static event Action<LevelInitializingData> OnLevelInitializing;
    public static event Action OnLevelStarting;
    public static event Action OnCustomerArriving;
    public static event Action OnCustomerItemScanning;
    public static event Action OnCustomerItemDealingStart;
    public static event Action<CustomerInputType> OnCustomerItemDealingEnd;
    public static event Action OnCustomerLeavingShop;
    public static event Action<GameplayData> OnLevelEnding;
    public static event Action<GameState> OnChangeGameState;

    public GameplayData gameplayData;
    public LevelInitializingData levelInitializingData;
    [HideInInspector]
    public CustomerInputType customerInputType;
    [HideInInspector]
    public GameState gameState;

    public virtual void Awake()
    {
        instance = this;
        gameplayData = new GameplayData();
        levelInitializingData = new LevelInitializingData();
    }

    public virtual void ChangeGameState(GameState gameState)
    {
        this.gameState = gameState;
        OnChangeGameState?.Invoke(gameState);

        switch (gameState)
        {
            case GameState.NONE:
                None();
                break;
            case GameState.IDLE:
                Idle();
                break;
            case GameState.LEVEL_INITIALIZING:
                LevelInitializing();
                break;
            case GameState.LEVEL_STARTING:
                LevelStarting();
                break;
            case GameState.CUSTOMER_ARRIVING:
                CustomerArriving();
                break;
            case GameState.CUSTOMER_ITEM_SCANNING:
                CustomerItemScanning();
                break;
            case GameState.CUSTOMER_ITEM_DEALING_START:
                CustomerItemDealingStart();
                break;
            case GameState.CUSTOMER_ITEM_DEALING_END:
                CustomerItemDealingEnd();
                break;
            case GameState.CUSTOMER_LEAVING_SHOP:
                CustomerLeavingShop();
                break;
            case GameState.LEVEL_ENDING:
                LevelEnding();
                break;
        }
    }

    public virtual void None()
    {
        OnNone?.Invoke();
    }

    public virtual void Idle()
    {
        OnIdle?.Invoke(gameplayData);
    }

    public virtual void LevelInitializing()
    {
        OnLevelInitializing?.Invoke(levelInitializingData);
    }

    public virtual void LevelStarting()
    {
        OnLevelStarting?.Invoke();
    }

    public virtual void CustomerArriving()
    {
        OnCustomerArriving?.Invoke();
    }

    public virtual void CustomerItemScanning()
    {
        OnCustomerItemScanning?.Invoke();
    }

    public virtual void CustomerItemDealingStart()
    {
        OnCustomerItemDealingStart?.Invoke();
    }

    public virtual void CustomerItemDealingEnd()
    {
        OnCustomerItemDealingEnd?.Invoke(customerInputType);
    }

    public virtual void CustomerLeavingShop()
    {
        OnCustomerLeavingShop?.Invoke();
    }

    public virtual void LevelEnding()
    {
        OnLevelEnding?.Invoke(gameplayData);
    }

}
