﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    private static LevelManager instance;
    private int currentLevel;
    private string GAME_LEVEL_PREFERENCE_KEY = "Game_Level_Pref";

    void Awake()
    {
        Singleton();
        currentLevel = PlayerPrefs.GetInt(GAME_LEVEL_PREFERENCE_KEY, 0);
        if (currentLevel == 0)
        {
            currentLevel = 1;
        }
        //Debug.LogError(currentLevel + " Cur LEv");
    }
    
   
    private void Singleton()
    {
        if (instance != null)
        {
            Destroy(gameObject); return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }
    
    
    
    public static LevelManager Instance
    {
        get
        {
            return instance;
        }
    }

    public void SetGameLevelAfterGameOver()
    {
        PlayerPrefs.SetInt(GAME_LEVEL_PREFERENCE_KEY, currentLevel);
    }
    public void IncreaseGameLevel()
    {
        currentLevel++;
        //Debug.LogError(currentLevel + " CurrentLevel");
        PlayerPrefs.SetInt(GAME_LEVEL_PREFERENCE_KEY, currentLevel);

        GameManagerGM.Instance.IncreaseNewGameLevel();
    }
    public void DecreaseGameLevel()
    {
        currentLevel--;
        PlayerPrefs.SetInt(GAME_LEVEL_PREFERENCE_KEY, currentLevel);
    }
    

    public int GetCurrentLevel()
    {
        currentLevel = PlayerPrefs.GetInt(GAME_LEVEL_PREFERENCE_KEY, 0);
        if (currentLevel == 0)
        {
            currentLevel = 1;
        }

        if (currentLevel > 5)
        {
            currentLevel = 1;
        }
       
        return currentLevel;
    }

    public string GetCurrentLevelWithLevelText()
    {
        currentLevel = PlayerPrefs.GetInt(GAME_LEVEL_PREFERENCE_KEY, 0);
        if (currentLevel == 0)
        {
            currentLevel = 1;
        }
       
        return "Lvl " + currentLevel;
    }
    public int GetNextLevel()
    {
        return currentLevel + 1;
    }

    public void ResetLevel()
    {
        currentLevel = 0;
        PlayerPrefs.DeleteKey(GAME_LEVEL_PREFERENCE_KEY);
    }

    public int GetLevelCompletionScore()
    {
        int t_LevelCompletionScore = 0;
        t_LevelCompletionScore = currentLevel * 10 + currentLevel * 3;
        

        return t_LevelCompletionScore;
    }
}