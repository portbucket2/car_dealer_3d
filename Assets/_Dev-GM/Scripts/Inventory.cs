﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    
}

[System.Serializable]
public class SavedClass
{
    public Dictionary<int, CarProfile> savedCarProfiles;

    public SavedClass()
    {
        savedCarProfiles = new Dictionary<int, CarProfile>();
    }
}
