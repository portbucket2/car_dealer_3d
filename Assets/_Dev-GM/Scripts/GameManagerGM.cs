﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class GameManagerGM : BaseCarDealerManager
{
   private LevelManagement m_LevelManagement;
   private Inventory m_Inventory;
   
   const string cdData = "/cdData.dat";
   private SavedClass m_SavedClass;
   
   
   private static GameManagerGM instance;

   public int levelNumber;
   private string GAME_LEVEL_PREFERENCE_NEW = "Game_Level_NEW";

   public int isMoneyGiven;
   public int initialMoney;
   private string IS_INIT_MONEY_GIVEN = "init_money_given";

   public int dayCount;
   private string GAME_DAY = "game_day";

   public static GameManagerGM Instance
   {
      get
      {
         return instance;
      }
   }
   
   public override void Awake()
   {
      base.Awake();
      Singleton();
      ChangeGameState(GameState.NONE);
      
      m_LevelManagement = GetComponent<LevelManagement>();
      m_Inventory = GetComponent<Inventory>();
      
     
      
   }

   private void Start()
   {
      m_SavedClass = new SavedClass();
      Init();
   }

   public void Init()
   {
      Debug.LogWarning(Application.persistentDataPath);
      LoadData();
      levelNumber = PlayerPrefs.GetInt(GAME_LEVEL_PREFERENCE_NEW, 1);

      isMoneyGiven = PlayerPrefs.GetInt(IS_INIT_MONEY_GIVEN, 0);

      dayCount = PlayerPrefs.GetInt(GAME_DAY, 1);
      
      if (isMoneyGiven == 0)
      {
         isMoneyGiven = 1;
         BasicCoinTracker.coinMan.ChangeBy(CurrencyType.COIN, initialMoney);
         PlayerPrefs.SetInt(IS_INIT_MONEY_GIVEN,isMoneyGiven);
      }
   }
   
   private void Singleton()
   {
      if (instance != null)
      {
         Destroy(gameObject); return;
      }
      instance = this;
      DontDestroyOnLoad(gameObject);
   }

   #region Public Functions

   public void ReadCSVAgain()
   {
      m_LevelManagement.ReadCSVAgain();

      dayCount++;
      PlayerPrefs.SetInt(GAME_DAY,dayCount);
   }
   
   public List<LevelData> GetAllLevelData()
   {
      if (m_LevelManagement != null)
      {
         return m_LevelManagement.GetAllLevelData();
      }

      return null;
   }

   public LevelData GetLevelData(int t_LevelNumber)
   {
      if (m_LevelManagement != null)
      {
        return m_LevelManagement.GetLevelData(t_LevelNumber);
      }

      return null;
   }

   public List<CarProfile> GetAllCarProfile()
   {
      if (m_LevelManagement != null)
      {
         return m_LevelManagement.GetAllCarProfiles();
      }

      return null;
   }

   public CarProfile GetCarProfile(int t_CarID)
   {
      if (m_LevelManagement != null)
      {
         return m_LevelManagement.GetCarProfile(t_CarID);
      }

      return null;
   }
   
   #endregion

   #region Data Saving
   public void SaveData()
   {
      BinaryFormatter bf = new BinaryFormatter();
      FileStream file = File.Create(Application.persistentDataPath + cdData);
      bf.Serialize(file, m_SavedClass);
      file.Close();
   }

   public void LoadData()
   {
      BinaryFormatter bf = new BinaryFormatter();

      if (File.Exists(Application.persistentDataPath + cdData))
      {
         FileStream file = File.Open(Application.persistentDataPath + cdData, FileMode.Open);
         m_SavedClass = (SavedClass)bf.Deserialize(file);
         file.Close();
      }
      
      Debug.Log(m_SavedClass.savedCarProfiles.Count + " FROPM SAVED DATA");
   }

   public CarProfile GetASingleCarProfileWithID(int t_ID)
   {
      CarProfile t_CarProfile = new CarProfile();

      if (m_SavedClass.savedCarProfiles.ContainsKey(t_ID))
      {
         t_CarProfile = m_SavedClass.savedCarProfiles[t_ID];
      }

      return t_CarProfile;
   }

   public List<CarProfile> GetAllSavedCarasAList()
   {
      List<CarProfile> t_CarProfileList = new List<CarProfile>();

      foreach (var carProfile in m_SavedClass.savedCarProfiles)
      {
         t_CarProfileList.Add(carProfile.Value);
         Debug.LogError(carProfile.Value.levelNumber + " LEVEL NUMBER SAVED AS CAR PROFILE" );
         Debug.LogError(carProfile.Key + " Key saved here");
      }

      return t_CarProfileList;
   }
   public void AddCarToSave(CarProfile t_CarProfile,int levelNumber)
   {
     // if (!m_SavedClass.savedCarProfiles.ContainsKey(t_CarProfile.id))
      //{
      //   m_SavedClass.savedCarProfiles.Add(t_CarProfile.id,t_CarProfile);
     // }
     // else
     // {
      //   m_SavedClass.savedCarProfiles[t_CarProfile.id] = t_CarProfile;
     // }

     //Add unlimited cars

     t_CarProfile.levelNumber = levelNumber;
     
     
     //Debug.LogError(t_CarProfile.levelNumber + " Car profile level number");
    // Debug.LogError(levelNumber + " incoming level number");
      if (!m_SavedClass.savedCarProfiles.ContainsKey(levelNumber))
      {
         m_SavedClass.savedCarProfiles.Add(levelNumber,t_CarProfile);
         Debug.LogError("Added car profile KEY -- " +levelNumber);
      }
      else
      {
         m_SavedClass.savedCarProfiles[levelNumber] = t_CarProfile;
         Debug.LogError("Modified car profile KEY -- "+levelNumber);
        Debug.LogError("Modile Car profile Level Number " +m_SavedClass.savedCarProfiles[levelNumber].levelNumber);
      }
     
   }

   public void RemoveCarFromSaved(CarProfile t_CarProfile,int levelNumber)
   {
      if (m_SavedClass.savedCarProfiles.ContainsKey(levelNumber))
      {
         m_SavedClass.savedCarProfiles.Remove(levelNumber);
      }
   }
   

   #endregion

   public void IncreaseNewGameLevel()
   {
      levelNumber++;
      PlayerPrefs.SetInt(GAME_LEVEL_PREFERENCE_NEW, levelNumber);
   }
   
   public void ResetLevel()
   {
      levelNumber = 0;
      PlayerPrefs.DeleteKey(GAME_LEVEL_PREFERENCE_NEW);
   }
}
