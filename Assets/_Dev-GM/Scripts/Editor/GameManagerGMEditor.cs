﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GameManagerGM))]
public class GameManagerGMEditor : Editor
{
    private GameManagerGM m_LevelManager;

    private void OnEnable()
    {
        m_LevelManager = (GameManagerGM) target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
      
        GUILayout.BeginHorizontal();
        GUILayout.Label("Current Level : "+m_LevelManager.levelNumber);

        if (GUILayout.Button("Increase"))
        {
            
        }
        if (GUILayout.Button("Decrease"))
        {
            
        }

        if (GUILayout.Button("Reset Level"))
        {
            m_LevelManager.ResetLevel();
        }
        GUILayout.EndHorizontal();
    }
}
