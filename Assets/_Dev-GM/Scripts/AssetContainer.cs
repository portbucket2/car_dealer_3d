﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetContainer : MonoBehaviour
{
   public List<CarAssetClass> carAssetList;
   public List<CharacterAssetClass> characterAssetList;
   
   
   
   private Dictionary<CarCategory, CarAssetClass> m_CarAssetDictionary;
   private Dictionary<CustomerType, CharacterAssetClass> m_CharacterAssetDictionary;
   

   
   private static AssetContainer instance;

   public static AssetContainer Instance
   {
      get
      {
         return instance;
      }
   }
   private void Singleton()
   {
      if (instance != null)
      {
         Destroy(gameObject); return;
      }
      instance = this;
      DontDestroyOnLoad(gameObject);
   }
   

   private void Awake()
   {
      Singleton();
      m_CarAssetDictionary = new Dictionary<CarCategory, CarAssetClass>();
      m_CharacterAssetDictionary = new Dictionary<CustomerType, CharacterAssetClass>();
      
      GenerateCarAssetDictionary();
      GenerateCharacterAssetDictionary();
   }

   private void GenerateCarAssetDictionary()
   {
      for (int i = 0; i < carAssetList.Count; i++)
      {
         m_CarAssetDictionary.Add(carAssetList[i].carCategory,carAssetList[i]);
      }
   }

   private void GenerateCharacterAssetDictionary()
   {
      for (int i = 0; i < characterAssetList.Count; i++)
      {
            if(m_CharacterAssetDictionary.ContainsKey(characterAssetList[i].customerType))
            {

            }
            else
            {
                m_CharacterAssetDictionary.Add(characterAssetList[i].customerType, characterAssetList[i]);
            }
         
      }
   }

   #region Public Functions

   public CarAssetClass GetCarWithCategory(CarCategory t_CarCategory)
   {
      CarAssetClass t_CarAsset = new CarAssetClass();

      if (m_CarAssetDictionary.ContainsKey(t_CarCategory))
      {
         t_CarAsset = m_CarAssetDictionary[t_CarCategory];
      }

      return t_CarAsset;
   }

   public CharacterAssetClass GetCharacterWithCategory(CustomerType t_CustomerType)
   {
      CharacterAssetClass t_Character = new CharacterAssetClass();
      if (m_CharacterAssetDictionary.ContainsKey(t_CustomerType))
      {
         t_Character = m_CharacterAssetDictionary[t_CustomerType];
      }

      return t_Character;
   }

    public CharacterAssetClass GetCharacterWithNumber(int t_CustomerNumber)
    {
        CharacterAssetClass t_Character = new CharacterAssetClass();
        t_Character = characterAssetList[t_CustomerNumber-1];

        return t_Character;
    }

    #endregion
}
