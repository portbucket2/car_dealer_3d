﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManagement : MonoBehaviour
{
    public TextAsset csvAsset;


    private Dictionary<int, LevelData> m_LevelDataDictionary;
    private List<LevelData> m_LevelDataList;

    private List<CarProfile> m_CarProfileList;
    private Dictionary<int, CarProfile> m_CarProfileDictionary;

    private void Awake()
    {
        m_LevelDataDictionary = new Dictionary<int, LevelData>();
        m_LevelDataList = new List<LevelData>();
        
        m_CarProfileList = new List<CarProfile>();
        m_CarProfileDictionary = new Dictionary<int, CarProfile>();
    }

    void Start()
    {
       ReadCSVData();
       CreateDataDictionary();
       //CreateCarProfileDictionary();

       //GetLevelData(3);
    }

    public void ReadCSVAgain()
    {
        m_LevelDataDictionary = new Dictionary<int, LevelData>();
        m_LevelDataList = new List<LevelData>();
        
        m_CarProfileList = new List<CarProfile>();
        m_CarProfileDictionary = new Dictionary<int, CarProfile>();
        
        ReadCSVData();
        CreateDataDictionary();
        
        
        Debug.LogWarning("READ CSV AGAIN !!!!!!");
    }
    
   

    private void ReadCSVData()
    {
        List<List<string>> csvData =  CSVOperation.GetCSVData(csvAsset,new List<int>(){0,1,2,3,4,5,6,7,8,9,10,11,12});
        int t_DataLenth = -1;
        int t_ColumnLenth = csvData.Count;
       // Debug.LogError(t_ColumnLenth);
        
        for (int i = 0; i < csvData.Count; i++)
        {
            t_DataLenth = csvData[i].Count;
            break;
        }
        
        //Debug.LogError(t_DataLenth + " ---- "+t_ColumnLenth);
        
        for (int i = 1; i < t_DataLenth; i++)
        {
            LevelData t_LevelData = new LevelData();
            CarProfile t_CarProfile = new CarProfile();
            
            for (int j = 0; j < t_ColumnLenth; j++)
            {
               
                if (csvData[j][0] == "Car Id")
                {
                    t_CarProfile.id = int.Parse(csvData[j][i]);
                }
                if (csvData[j][0] == "Car Name")
                {
                    t_CarProfile.name = csvData[j][i];
                }
                if (csvData[j][0] == "Asking Price")
                {
                    t_CarProfile.askingPrice = int.Parse(csvData[j][i]);
                }
                if (csvData[j][0] == "Current Value")
                {
                    t_CarProfile.currentPrice = int.Parse(csvData[j][i]);
                }
                if (csvData[j][0] == "Level")
                {
                    t_LevelData.levelNumber = int.Parse(csvData[j][i]);
                }
                if (csvData[j][0] == "Customer Type")
                {
                    t_LevelData.customerType = ParseEnum<CustomerType>(csvData[j][i]);
                }
                if (csvData[j][0] == "Customer Mode")
                {
                    t_LevelData.customerMode = ParseEnum<CustomerMode>(csvData[j][i]);
                }
                if (csvData[j][0] == "Car Category")
                {
                    t_LevelData.carCategory = ParseEnum<CarCategory>(csvData[j][i]);
                    t_CarProfile.carCategory = ParseEnum<CarCategory>(csvData[j][i]);
                }
                if (csvData[j][0] == "Body Meter")
                {
                    t_CarProfile.bodyMeter = int.Parse(csvData[j][i]);
                }
                if (csvData[j][0] == "Performance Meter")
                {
                    t_CarProfile.performanceMeter = int.Parse(csvData[j][i]);
                }
                if (csvData[j][0] == "Value increase on Body Repair")
                {
                    t_CarProfile.priceByBodyRepair = int.Parse(csvData[j][i]);
                }
                if (csvData[j][0] == "Value Increase on Performance Repair")
                {
                    t_CarProfile.priceByPerformanceRepair = int.Parse(csvData[j][i]);
                }
               
                if (csvData[j][0] == "Restored Value")
                {
                    t_CarProfile.priceRestoredFinal = int.Parse(csvData[j][i]);
                    //Debug.Log(t_CarProfile.priceRestoredFinal + "  ---- Never Comes Here. WTF");
                }
               
            }
            
            t_LevelData.carProfile = t_CarProfile;
            m_LevelDataList.Add(t_LevelData);
            m_CarProfileList.Add(t_CarProfile);
        }

        for (int i = 0; i < m_LevelDataList.Count; i++)
        {
            Debug.Log(m_LevelDataList[i].carProfile.id + "/" +
            m_LevelDataList[i].carProfile.name + "/" +
            m_LevelDataList[i].carProfile.askingPrice + "/" +
            m_LevelDataList[i].carProfile.currentPrice+ "/" +
            m_LevelDataList[i].customerMode + "/" +
            m_LevelDataList[i].customerType+ "/" +
            m_LevelDataList[i].carCategory + "/" +
            m_LevelDataList[i].levelNumber+ "/" +
            m_LevelDataList[i].carProfile.bodyMeter+ "/" +
            m_LevelDataList[i].carProfile.performanceMeter+ "/" +
            m_LevelDataList[i].carProfile.priceByBodyRepair+ "/" +
            m_LevelDataList[i].carProfile.priceByPerformanceRepair+ "/" +
            m_LevelDataList[i].carProfile.priceRestoredFinal+"/"
            );
        }
    }
    
    private void CreateDataDictionary()
    {
        if (m_LevelDataList != null && m_LevelDataList.Count > 0)
        {
            for (int i = 0; i < m_LevelDataList.Count; i++)
            {
                m_LevelDataDictionary.Add(m_LevelDataList[i].levelNumber,m_LevelDataList[i]);
            }
        }
    }
    
    private void CreateCarProfileDictionary()
    {
        if (m_CarProfileList != null && m_CarProfileList.Count > 0)
        {
            for (int i = 0; i < m_CarProfileList.Count; i++)
            {
                m_CarProfileDictionary.Add(m_CarProfileList[i].id,m_CarProfileList[i]);
            }
        }
    }
    private T ParseEnum<T>(string value)
    {
        return (T) Enum.Parse(typeof(T), value, true);
    }


    #region Public Functions
    
    public List<LevelData> GetAllLevelData()
    {
        if (m_LevelDataList != null && m_LevelDataList.Count > 0)
        {
            return m_LevelDataList;
        }

        return null;
    }

    public LevelData GetLevelData(int t_LevelNumber)
    {
        LevelData t_LevelData = new LevelData();
        if (m_LevelDataDictionary.ContainsKey(t_LevelNumber))
        {
            t_LevelData = m_LevelDataDictionary[t_LevelNumber];
        }
        //Debug.LogError(t_LevelNumber + " imcoming when parsing LEvel data as a kety");
       // Debug.LogError(t_LevelData.carProfile.levelNumber + " imcoming when parsing LEvel data Level Number");
        t_LevelData.carProfile.levelNumber = 0;
        return t_LevelData;
    }

    public List<CarProfile> GetAllCarProfiles()
    {
        if (m_CarProfileList != null && m_CarProfileList.Count > 0)
        {
            return m_CarProfileList;
        }

        return null;
    }

    public CarProfile GetCarProfile(int t_CarId)
    {
        CarProfile t_CarProfile = new CarProfile();
        if (m_CarProfileDictionary.ContainsKey(t_CarId))
        {
            t_CarProfile = m_CarProfileDictionary[t_CarId];
        }

        return t_CarProfile;
    }
    
    #endregion


    private void DebugLog(LevelData t_LevelData)
    {
        Debug.Log(t_LevelData.carProfile.id + "/" +
                  t_LevelData.carProfile.name + "/" +
                  t_LevelData.carProfile.askingPrice + "/" +
                  t_LevelData.carProfile.currentPrice + "/" +
                  t_LevelData.customerMode + "/" +
                  t_LevelData.customerType+ "/" +
                  t_LevelData.carCategory + "/" +
                  t_LevelData.levelNumber
        );
    }
}
