﻿using UnityEngine;

public enum CustomerType
{
    ORDINARY,
    THIEF,
    VIP,
    ROBBER
}

public enum CustomerMode
{
    SELLER,
    BUYER,
}

public enum CarCategory
{
   
    SEDAN,
    MUSCLE,
    CONVERTIBLE,
    SPORTS,
    NONE
}



public enum AssetCategory
{
    VEHICLE,
    CHARACTER
}

public enum CustomerInputType
{
    ACCEPTED,
    DECLINEED,
    CALL_POLICE
}
public enum ProductCondition
{
    NON_POLISHED,
    POLISHED
}
public enum GameState
{
    NONE,
    IDLE,
    LEVEL_INITIALIZING,
    LEVEL_STARTING,
    CUSTOMER_ARRIVING,
    CUSTOMER_ITEM_SCANNING,
    CUSTOMER_ITEM_DEALING_START,
    CUSTOMER_ITEM_DEALING_END,
    CUSTOMER_LEAVING_SHOP,
    LEVEL_ENDING
}

public enum PerformanceGameType
{
    WIRING,
    GEAR,
    ROUND
}

[System.Serializable]
public class CarAssetClass
{
    public string name;
    public CarCategory carCategory;
    public GameObject brokenCarPrefab;
    public GameObject cleanCarPrefab;
    public Sprite carSprite;
}

[System.Serializable]
public class CharacterAssetClass
{
    public string name;
    public CustomerType customerType;
    public CustomerMode customerMode;
    public GameObject customerPrefab;
}



[System.Serializable]
public class LevelData
{
    public int levelNumber;
    public CustomerType customerType;
    public CustomerMode customerMode;
    public GameObject customerCarPrefab;
    public CarCategory carCategory;
    public CarProfile carProfile;
}

[System.Serializable]
public class CarProfile
{
    public int id;
    public string name;
    public string description;
    public CarCategory carCategory;
    public int bodyMeter;
    public int performanceMeter;
    public int askingPrice;
    public int currentPrice;
    public int priceByBodyRepair;
    public int priceByPerformanceRepair;
    public int priceRestoredFinal;
    public bool isPolished;
    public bool isPerformanceIncrease;
    public bool isRVWatched;
    public bool isSold;
    public int levelNumber;
    public int GetRestoredValue()
    { 
        if (isPolished && isPerformanceIncrease)
        {
            return currentPrice + priceByBodyRepair + priceByPerformanceRepair;
            
        }
        else if(isPerformanceIncrease && !isPolished)
        {
            return currentPrice + priceByPerformanceRepair;
        }
        else if(isPolished && !isPerformanceIncrease)
        {
            return currentPrice + priceByBodyRepair;
        }
        else
        {
            return currentPrice;
        }
    }

    public void IncreaseBodyMeter(int t_Value)
    {
        this.bodyMeter = t_Value;
    }

    public void IncreasePerformanceMeter(int t_Value)
    {
        performanceMeter = t_Value;
    }
}

public class ConstantManager
{    
}

public class GameplayData
{
}

public class LevelInitializingData
{
}

[System.Serializable]
public class MinigameSetup
{
    public string name;
    public GameObject minigameGO;
    public GameObject minigameCode;
    public GameObject minigameCam;
    public IMinigame engineMini;
    public GameObject prefab;
}
