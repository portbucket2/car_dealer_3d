﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EngineMinigame : MonoBehaviour,IMinigame
{
    public int m_CurrentBallIndex;
    [Range(0f, 5f)] public float fillSpeed;
    
    public List<RotateBall> rotateBallList;

    public List<Image> barImageList;


    public bool isReset;
    //public event Action OnEngineMinigameEnd;


    private void ResetEngine()
    {
        for (int i = 0; i < barImageList.Count; i++)
        {
            barImageList[i].fillAmount = 0;
        }

        for (int i = 0; i < rotateBallList.Count; i++)
        {
            rotateBallList[i].ResetPos();
        }

        m_CurrentBallIndex = 0;
    }
    
    private void Start()
    {
        StartMinigame();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            StartCoroutine(ManageBalls());
            Debug.Log("Down");
        }

        if (isReset)
        {
            Reset();
            isReset = false;
        }
    }

    private IEnumerator ManageBalls()
    {
        yield return new WaitForEndOfFrame();
        
        rotateBallList[m_CurrentBallIndex].isRotate = false;
        
        if (rotateBallList[m_CurrentBallIndex].IsCompletedCheckmark())
        {
            rotateBallList[m_CurrentBallIndex].SnapToMiddle();
            rotateBallList[m_CurrentBallIndex].SetColoredObject();
            float t_FillAmount = 0f;
            while (true)
            {
                t_FillAmount += Time.deltaTime *fillSpeed;
                barImageList[m_CurrentBallIndex].fillAmount = t_FillAmount;
                yield return new WaitForEndOfFrame();
                if (t_FillAmount > 1f)
                {
                    break;
                }
            }
            
            yield return new WaitForSeconds(0.3f);
            
            m_CurrentBallIndex++;

            if (m_CurrentBallIndex == rotateBallList.Count)
            {
                Debug.Log("Completed All");
                OnMinigameEnd?.Invoke();
            }
            else
            {
                rotateBallList[m_CurrentBallIndex].isRotate = true;
            }
        }
        else
        {
            yield return new WaitForSeconds(0.2f);
            rotateBallList[m_CurrentBallIndex].isRotate = true;
        }
    }

    private void StartMinigame()
    {
        rotateBallList[m_CurrentBallIndex].isRotate = true;
    }

    public void ActivateMinigame()
    {
        StartMinigame();
    }
    public void DeactivateMinigame()
    {
        
    }

    public void Reset()
    {
        ResetEngine();
    }

    public event Action OnMinigameEnd;
}
