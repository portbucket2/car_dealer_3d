﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateBall : MonoBehaviour
{
    public bool isRotate;
    [Range(0f, 10f)] public float rotateSpeed;
    public AngleStruct angleStruct;
    
    public GameObject target;
    public MeshRenderer connectedObject;
    public Material connectedMAT;

    public float snapRotation;

    public Material normalMAT;
    public Vector3 initPos;
    public Quaternion initRot;
    private void Awake()
    {
        initPos = transform.localPosition;
        initRot = transform.localRotation;
    }

    private void Update()
    {
        if (isRotate)
        {
            transform.RotateAround(target.transform.position, new Vector3(0,0,1),90 * Time.deltaTime*rotateSpeed);
        }
    }

    public bool IsCompletedCheckmark()
    {
        bool t_IsCheck = false;
        
        float t_CurrentAngles = GetAngles(transform.rotation.eulerAngles.z);

        Debug.Log(t_CurrentAngles);
        
        if (t_CurrentAngles < angleStruct.minAngle && t_CurrentAngles> angleStruct.maxAngle)
        {
            t_IsCheck = false;
        }
        else if (t_CurrentAngles > angleStruct.minAngle && t_CurrentAngles < angleStruct.maxAngle)
        {
            t_IsCheck = true;
        }
        else
        {
            t_IsCheck = false;
        }
        
        
        return t_IsCheck;
    }

    private float GetAngles(float t_GivenAngle)
    {
        float t_Angle = 0f;

        if (t_GivenAngle <= 180)
        {
            t_Angle = t_GivenAngle;
        }
        else
        {
            t_Angle = 360-t_GivenAngle;
        }
            
        return t_Angle;
    }

    public void SnapToMiddle()
    {
       transform.localPosition = new Vector3(transform.localPosition.x,0.3f,
           transform.localPosition.z);
       
       transform.localRotation = Quaternion.Euler(new Vector3(transform.localRotation.eulerAngles.x,
           transform.localRotation.eulerAngles.y,snapRotation));
    }

    public void SetColoredObject()
    {
        connectedObject.material = connectedMAT;
    }

    public void ResetPos()
    {
        transform.localPosition = initPos;
        transform.localRotation = initRot;
        connectedObject.material = normalMAT;
    }
    
}

[System.Serializable]
public struct AngleStruct
{
    public float minAngle;
    public float maxAngle;
}
