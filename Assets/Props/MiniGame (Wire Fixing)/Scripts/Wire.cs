﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wire : MonoBehaviour
{
    public SpriteRenderer wireEnd;
    public GameObject lightOn;
    Vector3 startPoint;
    Vector3 offset = new Vector3(0.2f, 0f, 0);
    // Start is called before the first frame update
    void Start()
    {
        startPoint = transform.parent.position + offset;
        
    }

    private void OnMouseDrag()
    {
        Vector3 newPos = GetWorldPositionOnPlane(Input.mousePosition, 0);
        
        
        Collider2D[] colliders = Physics2D.OverlapCircleAll(newPos, 0.2f);
        foreach(Collider2D collider in colliders)
        {
            if(collider.gameObject != gameObject)
            {
                UpdateWire(collider.transform.position);

                if(transform.parent.name.Equals(collider.transform.parent.name))
                {
                    Debug.Log("Name Same");
                    WireManager.Instance.WireCoonected(1);
                    collider.GetComponent<Place>() ?.Done();
                    Done();
                }
                return;
            }
            
            
        }

        UpdateWire(newPos + offset);
        
    }

    public Vector3 GetWorldPositionOnPlane(Vector3 screenPosition, float z)
    {
        Ray ray = Camera.main.ScreenPointToRay(screenPosition);
        Plane xy = new Plane(Vector3.forward, new Vector3(0, 0, z));
        float distance;
        xy.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }

    void Done()
    {
        lightOn.SetActive(true);
        Destroy(this);
    }
    private void OnMouseUp()
    {
        UpdateWire(startPoint + offset);
    }

    void UpdateWire(Vector3 newPos)
    {
        transform.position = newPos;

        Vector3 dir = newPos - startPoint;
        transform.right = dir * transform.lossyScale.x;

        float dist = Vector2.Distance(newPos, startPoint);
        wireEnd.size = new Vector2(dist, wireEnd.size.y);
    }
}
