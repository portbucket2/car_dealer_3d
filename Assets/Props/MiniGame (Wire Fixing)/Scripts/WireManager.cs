﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WireManager : MonoBehaviour,IMinigame
{
    static public WireManager Instance;
    public int wireCount;
    public GameObject winText;
    private int count = 0;
    [SerializeField]
    private List<GameObject> m_wires = new List<GameObject>();
    [SerializeField]
    private List<SpriteRenderer> m_wireExpeded = new List<SpriteRenderer>();


    private WorkshopUI workshopUI;
    
    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
    }

    public void WireCoonected(int points)
    {
        count = count + points;
        if(count == wireCount)
        {
            OnMinigameEnd?.Invoke();
            Debug.Log("Wire Minigame Ended!");
            //winText.SetActive(true);
            workshopUI.WireMinigameEnd();
        }
    }

    public void ActivateMinigame()
    {
    }

    public void DeactivateMinigame()
    {
    }

    public void Reset()
    {
        ResetWires();
    }

    public void ResetWires()
    {
        foreach (GameObject wire in m_wires)
        {
            wire.transform.localPosition = new Vector3(0.44f, 0f, 0f);
        }

        foreach (SpriteRenderer start in m_wireExpeded)
        {
            start.size = new Vector2(0.5132829f, 0.28f);
        }

    }

    public event Action OnMinigameEnd;

    public void Set(WorkshopUI workshopUI)
    {
        this.workshopUI = workshopUI;
    }
}
